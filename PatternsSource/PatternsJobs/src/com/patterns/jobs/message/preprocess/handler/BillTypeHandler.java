package com.patterns.jobs.message.preprocess.handler;

import java.util.Map;

import patterns.config.framework.database.utils.DBUtil;

import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public class BillTypeHandler extends AbstractPreProcessor {

	@Override
	public Map<String, Object> process() throws JobsException {
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String reservedFor = getRecord()[2];
			if (reservedFor.equals("I") || reservedFor.equals(""))
				getProcessContext().put("USED_FOR_IN_PATIENTS", "1");
			else
				getProcessContext().put("USED_FOR_IN_PATIENTS", "0");
			
			if (reservedFor.equals("O") || reservedFor.equals(""))
				getProcessContext().put("USED_FOR_OUT_PATIENTS", "1");
			else
				getProcessContext().put("USED_FOR_OUT_PATIENTS", "0");
		} catch (Exception e) {
			logger.logError("process() - Error - " + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
		} finally {
			dbUtil.reset();
		}
		return getProcessContext();
	}
}
