package com.patterns.jobs.message.preprocess.handler;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Map;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;

import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public class PersonDbOtherInfoHandler extends AbstractPreProcessor {

	@Override
	public Map<String, Object> process() throws JobsException {
		String newrel_code10 = RegularConstants.EMPTY_STRING;
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String title_code1 = getRecord()[1].trim();
			String title_code2 = getRecord()[2].trim();
			String title_code11 = getRecord()[11].trim();
			String title_code12 = getRecord()[12].trim();
			String title_code16 = getRecord()[16].trim();
			String title_code17 = getRecord()[17].trim();
			String rel_code10 = getRecord()[10].trim();
			String personId = getRecord()[0].trim();

			String[] coll = { title_code1, title_code2, title_code11, title_code12, title_code16, title_code17 };
			ArrayList<String> arr = new ArrayList<String>();
			for (int i = 0; i < coll.length; ++i)
				if (coll[i].isEmpty()) {
					arr.add(coll[i]);
				} else {
					String selectTitle_Code1 = "SELECT NEW_VALUE FROM COBOLJAVAMAP WHERE ENTITY_CODE=? AND TABLE_NAME=? AND COLUMN_NAME=? AND OLD_VALUE=?";
					dbUtil.reset();
					dbUtil.setMode(DBUtil.PREPARED);
					dbUtil.setSql(selectTitle_Code1);
					dbUtil.setString(1, getEntityCode());
					dbUtil.setString(2, "TITLE");
					dbUtil.setString(3, "TITLE_CODE");
					dbUtil.setString(4, coll[i]);
					ResultSet rset1 = dbUtil.executeQuery();
					if (rset1.next()) {
						arr.add(rset1.getString(1));

					} else {
						logger.logError("process() - Error - No Record In COBOLJAVAMAP");
						throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
					}
				}
			if (!rel_code10.isEmpty()) {
				String selectTitle_Code1 = "SELECT NEW_VALUE FROM COBOLJAVAMAP WHERE ENTITY_CODE=? AND TABLE_NAME=? AND COLUMN_NAME=? AND OLD_VALUE=?";
				dbUtil.reset();
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(selectTitle_Code1);
				dbUtil.setString(1, getEntityCode());
				dbUtil.setString(2, "RELATIONREF");
				dbUtil.setString(3, "RELREF_CODE");
				dbUtil.setString(4, rel_code10);
				ResultSet rset1 = dbUtil.executeQuery();
				if (rset1.next()) {
					newrel_code10 = rset1.getString(1).trim();
				} else {
					logger.logError("process() - Error - No Record In COBOLJAVAMAP");
					throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
				}
			}
			String personDbOtherInfo = null;
			if (getOperation().equals("W")) {
				personDbOtherInfo = "INSERT INTO PERSONOTHINFO (PARTITION_NO,PERSON_ID,LEGAL_CONN_REL_CODE,LEGAL_CONN_TITLE_CODE1,LEGAL_CONN_TITLE_CODE2,LEGAL_CONN_PERSON_NAME,DEPENDENT_FLAG,DEPEND_REL_CODE,DEPEND_TITLE1,DEPEND_TITLE2,DEPEND_PERSON_NAME,RELATIONSHIP_INFO,DECEASED_ENTD_ON,DECEASED_ENTD_BY,DECEASED_CAUSE,DECEASED_INFO,OMG_ADDRESS1,OMG_ADDRESS2,OMG_ADDRESS3,OMG_ADDRESS4,OMG_ADDRESS5,OMG_CONTACT1,OMG_CONTACT2,OMG_OP_NO,OMG_PDB_CAT) VALUES('1'," + personId + ",?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			} else {
				personDbOtherInfo = "UPDATE PERSONOTHINFO SET LEGAL_CONN_REL_CODE=?,LEGAL_CONN_TITLE_CODE1=?,LEGAL_CONN_TITLE_CODE2=?,LEGAL_CONN_PERSON_NAME=?,DEPENDENT_FLAG=?,DEPEND_REL_CODE=?,DEPEND_TITLE1=?,DEPEND_TITLE2=?,DEPEND_PERSON_NAME=?,RELATIONSHIP_INFO=?,DECEASED_ENTD_ON=?,DECEASED_ENTD_BY=?,DECEASED_CAUSE=?,DECEASED_INFO=?,OMG_ADDRESS1=?,OMG_ADDRESS2=?,OMG_ADDRESS3=?,OMG_ADDRESS4=?,OMG_ADDRESS5=?,OMG_CONTACT1=?,OMG_CONTACT2=?,OMG_OP_NO=?,OMG_PDB_CAT=? WHERE PARTITION_NO='1' AND PERSON_ID=" + personId + "";
			}
			dbUtil.reset();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(personDbOtherInfo);
			if (!newrel_code10.isEmpty()) {
				dbUtil.setString(1, newrel_code10);
			} else {
				dbUtil.setString(1, rel_code10);
			}
			dbUtil.setString(2, arr.get(2));
			dbUtil.setString(3, arr.get(3));
			dbUtil.setString(4, getRecord()[13].trim());
			dbUtil.setString(5, getRecord()[14].trim());
			dbUtil.setString(6, getRecord()[15].trim());
			dbUtil.setString(7, arr.get(4));
			dbUtil.setString(8, arr.get(5));
			dbUtil.setString(9, getRecord()[18].trim());
			dbUtil.setString(10, getRecord()[19].trim());
			dbUtil.setString(11, getRecord()[29].trim());
			dbUtil.setString(12, getRecord()[30].trim());
			dbUtil.setString(13, getRecord()[31].trim());
			dbUtil.setString(14, getRecord()[32].trim());
			dbUtil.setString(15, getRecord()[33].trim());
			dbUtil.setString(16, getRecord()[34].trim());
			dbUtil.setString(17, getRecord()[35].trim());
			dbUtil.setString(18, getRecord()[36].trim());
			dbUtil.setString(19, getRecord()[37].trim());
			dbUtil.setString(20, getRecord()[38].trim());
			dbUtil.setString(21, getRecord()[39].trim());
			if (!getRecord()[40].trim().isEmpty()) {
				dbUtil.setString(22, getRecord()[40].trim());
			} else {
				dbUtil.setString(22, RegularConstants.NULL);
			}
			dbUtil.setString(23, getRecord()[41].trim());
			dbUtil.executeUpdate();
			getProcessContext().put("TITLE_CODE1", arr.get(0));
			getProcessContext().put("TITLE_CODE2", arr.get(1));
		} catch (Exception e) {
			logger.logError("process() - Error - " + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
		} finally {
			dbUtil.reset();
		}
		return getProcessContext();
	}
}