package com.patterns.jobs.message.preprocess.handler;

import java.util.Map;

import patterns.config.framework.database.RegularConstants;

import com.patterns.interfaces.config.manager.bean.PreProcessorRecordConfigBean;
import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public class ManyToOneHandler extends AbstractPreProcessor {

	@Override
	public Map<String, Object> process() throws JobsException {
		try {
			StringBuffer result = new StringBuffer(RegularConstants.EMPTY_STRING);
			String value = RegularConstants.EMPTY_STRING;
			for (PreProcessorRecordConfigBean bean : getConfigDetails()) {
				int index = bean.getIndex();
				value = getRecord()[index];
				if (value != null) {
					result.append(value.trim());
				}
			}
			value = result.toString();
			getProcessContext().put(getSourceColumnName(), value);
		} catch (Exception e) {
			logger.logError("process() - Error - " + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
		} finally {
		}
		return getProcessContext();
	}
}
