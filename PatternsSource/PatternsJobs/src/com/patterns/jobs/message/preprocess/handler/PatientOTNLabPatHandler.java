package com.patterns.jobs.message.preprocess.handler;

import java.sql.ResultSet;
import java.util.Map;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;

import com.patterns.interfaces.config.manager.bean.PreProcessorRecordConfigBean;
import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public class PatientOTNLabPatHandler extends AbstractPreProcessor {

	@Override
	public Map<String, Object> process() throws JobsException {
		String regNumber = RegularConstants.EMPTY_STRING;
		String labPatCatCode = RegularConstants.EMPTY_STRING;
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String regCode = RegularConstants.EMPTY_STRING;
			for (PreProcessorRecordConfigBean bean : getConfigDetails()) {
				int index = bean.getIndex();
				String value = getRecord()[index];
				if (value != null) {
					regCode += value.trim();
				}
			}
			String selectRegNumber = "SELECT PATIENT_REG_NO FROM PATIENTREGOTN WHERE ENTITY_CODE=? AND PATIENT_REG_CODE=?";
			dbUtil.reset();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(selectRegNumber);
			dbUtil.setString(1, getEntityCode());
			dbUtil.setString(2, regCode);
			ResultSet rset = dbUtil.executeQuery();
			if (rset.next()) {
				regNumber = rset.getString(1);
			}
			if (regNumber.equals(RegularConstants.EMPTY_STRING)) {
				logger.logError("process() - Error - No record in PATIENTREGOTN");
				throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
			}

			dbUtil.reset();
			String selectLabPatCatCode = "SELECT LAB_PATCAT_CODE FROM PATIENTREG WHERE ENTITY_CODE=? AND PATIENT_REG_NO=?";
			dbUtil.reset();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql(selectLabPatCatCode);
			dbUtil.setString(1, getEntityCode());
			dbUtil.setString(2, regNumber);
			ResultSet rset1 = dbUtil.executeQuery();
			if (rset1.next()) {
				labPatCatCode = rset1.getString(1);
			} else {
				logger.logError("process() - Error - No record in PATIENTREG");
				throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
			}
			dbUtil.reset();
			getProcessContext().put("LAB_PATCAT_CODE", labPatCatCode);
			getProcessContext().put(getSourceColumnName(), regNumber);
		} catch (Exception e) {
			logger.logError("process() - Error - " + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
		} finally {
			dbUtil.reset();
		}
		return getProcessContext();
	}
}