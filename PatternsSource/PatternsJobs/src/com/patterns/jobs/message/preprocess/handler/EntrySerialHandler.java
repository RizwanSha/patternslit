package com.patterns.jobs.message.preprocess.handler;

import java.util.Map;
import patterns.config.framework.database.utils.DBUtil;
import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public class EntrySerialHandler extends AbstractPreProcessor {

	@Override
	public Map<String, Object> process() throws JobsException {
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String dtlSl = getRecord()[3];
			int sl = 0;
			int entrySl = 0;
			sl = Integer.parseInt(dtlSl);
			entrySl = sl / 500;
			getProcessContext().put(getSourceColumnName(), String.valueOf(entrySl));

		} catch (Exception e) {
			logger.logError("process() - Error - " + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
		} finally {
			dbUtil.reset();
		}
		return getProcessContext();
	}
}
