package com.patterns.jobs.message.preprocess.handler;

import java.util.Map;

import patterns.config.framework.database.RegularConstants;

import com.patterns.interfaces.config.manager.bean.PreProcessorRecordConfigBean;
import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public class RemovingLeadingZero extends AbstractPreProcessor {

	@Override
	public Map<String, Object> process() throws JobsException {
		String value = RegularConstants.EMPTY_STRING;
		try {
			for (PreProcessorRecordConfigBean bean : getConfigDetails()) {
				int index = bean.getIndex();
				value = getRecord()[index].trim();
			}
			String valueWithOutZero = Integer.valueOf(value).toString();
			getProcessContext().put(getSourceColumnName(), valueWithOutZero);
		} catch (Exception e) {
			logger.logError("process() - Error - " + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
		}
		return getProcessContext();
	}
}
