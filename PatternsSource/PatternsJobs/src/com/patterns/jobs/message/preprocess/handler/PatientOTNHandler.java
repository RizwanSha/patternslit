package com.patterns.jobs.message.preprocess.handler;

import java.sql.ResultSet;
import java.util.Map;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.database.utils.DBUtil;

import com.patterns.interfaces.config.manager.bean.PreProcessorRecordConfigBean;
import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public class PatientOTNHandler extends AbstractPreProcessor {

	@Override
	public Map<String, Object> process() throws JobsException {
		String regNumber = null;
		DBUtil dbUtil = getDbContext().createUtilInstance();
		try {
			String regCode = RegularConstants.EMPTY_STRING;
			for (PreProcessorRecordConfigBean bean : getConfigDetails()) {
				int index = bean.getIndex();
				String value = getRecord()[index];
				if (value != null) {
					regCode += value.trim();
				}
			}
			if (getOperation().equals("W")) {
				String selectRegNumber = "SELECT IFNULL(MAX(PATIENT_REG_NO),0)+1 FROM PATIENTREGOTN WHERE ENTITY_CODE=?";
				dbUtil.reset();
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(selectRegNumber);
				dbUtil.setString(1, getEntityCode());
				ResultSet rset = dbUtil.executeQuery();
				if (rset.next()) {
					regNumber = rset.getString(1);
				}
				dbUtil.reset();

				/* CHANGED BY PAVAN ON 17032015 */
				String insertPatientOTN = "INSERT INTO PATIENTREGOTN (ENTITY_CODE,PATIENT_REG_CODE,PATIENT_REG_NO) VALUES (?,?,?)";
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(insertPatientOTN);
				dbUtil.setString(1, getEntityCode());
				dbUtil.setString(2, regCode);
				dbUtil.setString(3, regNumber);
				dbUtil.executeUpdate();
				dbUtil.reset();
			} else {
				/* CHANGED BY PAVAN ON 17032015 */
				String selectRegNumber = "SELECT PATIENT_REG_NO FROM PATIENTREGOTN WHERE ENTITY_CODE=? AND PATIENT_REG_CODE=?";
				dbUtil.reset();
				dbUtil.setMode(DBUtil.PREPARED);
				dbUtil.setSql(selectRegNumber);
				dbUtil.setString(1, getEntityCode());
				dbUtil.setString(2, regCode);
				ResultSet rset = dbUtil.executeQuery();
				if (rset.next()) {
					regNumber = rset.getString(1);
				}
				dbUtil.reset();

				if (regNumber.equals(RegularConstants.EMPTY_STRING)) {
					logger.logError("process() - Error - No record in PATIENTREGOTN");
					throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
				}
			}
			getProcessContext().put(getSourceColumnName(), regNumber);

		} catch (Exception e) {
			logger.logError("process() - Error - " + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
		} finally {
			dbUtil.reset();
		}
		return getProcessContext();
	}
}
