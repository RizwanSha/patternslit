package com.patterns.jobs.message.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.quartz.JobExecutionException;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;

import com.patterns.interfaces.beans.EmailBean;
import com.patterns.interfaces.beans.SmsInterfaceBean;
import com.patterns.interfaces.handler.EmailDispatchHandler;
import com.patterns.interfaces.handler.SmsDispatchHandler;
import com.patterns.jobs.application.monitor.ApplicationValidator;
import com.patterns.jobs.bean.TemplateBean;
import com.patterns.jobs.bean.loader.TemplateLoader;
import com.patterns.jobs.exceptions.JobsException;

public class CommunicationHandler extends AbstractMessageHandler {
	private ApplicationLogger logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());

	public DTObject dispatchEMail(DBContext dbContext ,DTObject inputDTO) {
		logger.logDebug("process() - Begin");
		ApplicationValidator validator = new ApplicationValidator();
		DTObject formDTO = new DTObject();
		formDTO.reset();
		try {
			// String emailInterfaceCode = RegularConstants.EMPTY_STRING;
			
			String templateCode = inputDTO.get("TEMPLATE_CODE");
			TemplateBean templateBean = TemplateLoader.getInstance().getTemplate(inputDTO.get("ENTITY_CODE"), templateCode);
			formDTO = validator.processMessageTemplate(inputDTO, templateBean);
			EmailBean emailBean = new EmailBean();
			emailBean.setEmailInterfaceCode(inputDTO.get("INTERFACE_CODE"));
			emailBean.setEmailBody(formDTO.get("BODY"));
			emailBean.setEmailSubject(formDTO.get("SUBJECT"));
			emailBean.setEmailID(inputDTO.get("EMAIL_ID"));
			
			if(inputDTO.getObject("ATTACHMENT")!=null){
				@SuppressWarnings("unchecked")
				Map <String,String> attachments = (HashMap<String, String>) inputDTO.getObject("ATTACHMENT");
				emailBean.setAttachmentList(attachments);
			}
			EmailDispatchHandler interfaceHandler = new EmailDispatchHandler();
			interfaceHandler.setEntityCode(inputDTO.get("ENTITY_CODE"));
			interfaceHandler.setPojoInstance(emailBean);
			interfaceHandler.process(dbContext);
			formDTO.reset();
			formDTO.set("RESULT", "SUCCESS");
			return formDTO;
		} catch (Exception e) {
			formDTO.set("RESULT", "FAILURE");
			e.printStackTrace();
			logger.logError("execute() Exception" + e.getLocalizedMessage());
		} finally {
			validator.close();
		}
		logger.logDebug("process() - End");
		return formDTO;
	}
	
	public DTObject dispatchSMS(DBContext dbContext,DTObject inputDTO) {
		logger.logDebug("process() - Begin");
		ApplicationValidator validator = new ApplicationValidator();
		DTObject formDTO = new DTObject();
		formDTO.reset();
		try {
			// String emailInterfaceCode = RegularConstants.EMPTY_STRING;
			
			String templateCode = inputDTO.get("TEMPLATE_CODE");
			TemplateBean templateBean = TemplateLoader.getInstance().getTemplate(inputDTO.get("ENTITY_CODE"), templateCode);
			formDTO = validator.processMessageTemplate(inputDTO, templateBean);
			SmsInterfaceBean smsBean = new SmsInterfaceBean();
            smsBean.setSmsInterfaceCode(inputDTO.get("INTERFACE_CODE"));
            smsBean.setTextMessage(formDTO.get("BODY"));
            smsBean.setMobileNumber(inputDTO.get("PHONE"));
            SmsDispatchHandler interfaceHandler = new SmsDispatchHandler();
            interfaceHandler.setEntityCode(inputDTO.get("ENTITY_CODE"));
            interfaceHandler.setPojoInstance(smsBean);
            interfaceHandler.process(dbContext);
			formDTO.reset();
			formDTO.set("RESULT", "SUCCESS");
			return formDTO;
		} catch (Exception e) {
			formDTO.set("RESULT", "FAILURE");
			e.printStackTrace();
			logger.logError("execute() Exception" + e.getLocalizedMessage());
		} finally {
			validator.close();
		}
		logger.logDebug("process() - End");
		return formDTO;
	}
	
	
	
	// ADD ON 25-02-2017 START
		@SuppressWarnings("unchecked")
		public DTObject dispatchEMailWithInventoryNumber(DBContext dbContext, DTObject inputDTO) {
			logger.logDebug("process() - Begin");
			ApplicationValidator validator = new ApplicationValidator();
			DTObject formDTO = new DTObject();
			formDTO.reset();
			try {
				// String emailInterfaceCode = RegularConstants.EMPTY_STRING;
				String templateCode = inputDTO.get("TEMPLATE_CODE");
				TemplateBean templateBean = TemplateLoader.getInstance().getTemplate(inputDTO.get("ENTITY_CODE"), templateCode);
				formDTO = validator.processMessageTemplate(inputDTO, templateBean);
				EmailBean emailBean = new EmailBean();
				emailBean.setEmailInterfaceCode(inputDTO.get("INTERFACE_CODE"));
				emailBean.setEmailBody(formDTO.get("BODY"));
				emailBean.setEmailSubject(formDTO.get("SUBJECT"));

				// emailBean.setEmailID(inputDTO.get("EMAIL_ID"));
				if (inputDTO.getObject("ATTACHMENT") != null) {
					Map<String, String> attachments = (HashMap<String, String>) inputDTO.getObject("ATTACHMENT");
					emailBean.setAttachmentList(attachments);
				}

				if (inputDTO.getObject("TO_EMAIL_ID_LIST") != null)
					emailBean.setToEmailIDList((ArrayList<String>) inputDTO.getObject("TO_EMAIL_ID_LIST"));
				if (inputDTO.getObject("CC_EMAIL_ID_LIST") != null)
					emailBean.setCcEmailIDList((ArrayList<String>) inputDTO.getObject("CC_EMAIL_ID_LIST"));
				if (inputDTO.getObject("BCC_EMAIL_ID_LIST") != null)
					emailBean.setBccEmailIDList((ArrayList<String>) inputDTO.getObject("BCC_EMAIL_ID_LIST"));
				

				EmailDispatchHandler interfaceHandler = new EmailDispatchHandler();
				interfaceHandler.setEntityCode(inputDTO.get("ENTITY_CODE"));
				interfaceHandler.setPojoInstance(emailBean);
				interfaceHandler.processWithInventoryNumber(dbContext, inputDTO.get("INVENTORY_NO"));
				formDTO.reset();
				formDTO.set("RESULT", "SUCCESS");
				return formDTO;
			} catch (Exception e) {
				formDTO.set("RESULT", "FAILURE");
				e.printStackTrace();
				logger.logError("execute() Exception" + e.getLocalizedMessage());
			} finally {
				validator.close();
			}
			logger.logDebug("process() - End");
			return formDTO;
		}

		// ADD ON 25-02-2017 END


	@Override
	public void process() throws JobExecutionException, JobsException {
		// TODO Auto-generated method stub
		
	}
	
}
