package com.patterns.jobs.message.handler;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import patterns.config.framework.loggers.ApplicationLogger;

import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;
import com.patterns.jobs.message.handler.bean.RecordParser;

public class GenericInwardMessageHandler extends AbstractMessageHandler {
	private ApplicationLogger logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());

	private final int RECORDOPR_START_INDEX = 50;
	private final int RECORDOPR_END_INDEX = 51;

	public void process() throws JobsException {
		logger.logDebug("process() - Begin");

		if (getRecordList().size() > 0) {
			Map<String, List<Map<String, Object>>> tableDataMap = new HashMap<String, List<Map<String, Object>>>();
			String operationType = "";
			for (String recordContent : getRecordList()) {
				String recordOperation = recordContent.substring(RECORDOPR_START_INDEX, RECORDOPR_END_INDEX).trim();
				if (!recordOperation.equals("W") && !recordOperation.equals("R")) {
					logger.logError("process() - Error - Operation Not allowed " + recordOperation);
					throw new JobsException(JobsErrorCodes.OPERATION_E);
				}
				RecordParser parser = new RecordParser(getDbContext(), recordContent, getEntityCode(),
						getProcessingCode());
				Map<String, Object> recordValue = parser.parseRecordDetails();
				recordValue.put("PROGRAM_ID", getProgramId());
				recordValue.put("ENTITY_CODE", getEntityCode());

				if (parser.getTableType().equals("S")) {
					RecordConfiguration configuration = new RecordConfiguration(parser.getTableName(), getDbContext());
					configuration.getInternalStore().putAll(recordValue);
					if (parser.getOperationType().equals("W")) {
						configuration.setNew(true);
					} else if (parser.getOperationType().equals("R")) {
						configuration.setNew(false);
					}
					configuration.save();
				} else {
					if (!tableDataMap.containsKey(parser.getTableName())) {
						List<Map<String, Object>> recordList = new LinkedList<Map<String, Object>>();
						recordList.add(recordValue);
						tableDataMap.put(parser.getTableName(), recordList);
						operationType = parser.getOperationType();
					} else {
						List<Map<String, Object>> recordList = tableDataMap.get(parser.getTableName());
						recordList.add(recordValue);
						tableDataMap.put(parser.getTableName(), recordList);
					}
				}
			}

			for (String tableName : tableDataMap.keySet()) {
				List<Map<String, Object>> recordList = tableDataMap.get(tableName);
				RecordConfiguration configuration = new RecordConfiguration(tableName, getDbContext());
				configuration.setOperationType(operationType);
				/* CHANGED BY PAVAN ON 17032015 BEG */
				if (operationType.equals("R")) {
					configuration.deleteByKeyFields(recordList);
				}
				/* CHANGED BY PAVAN ON 17032015 END */
				configuration.setNew(true);
				configuration.save(recordList);
			}
		}
		logger.logDebug("process() - End");
	}
}
