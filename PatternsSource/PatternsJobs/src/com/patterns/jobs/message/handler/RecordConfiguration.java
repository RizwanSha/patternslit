package com.patterns.jobs.message.handler;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import patterns.config.framework.database.BackOfficeConstants;
import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.objects.TableInfo;
import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBInfo;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;
import patterns.config.framework.web.BackOfficeFormatUtils;

import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public class RecordConfiguration {
	protected final ApplicationLogger logger;
	String tableName = null;
	private String[] keyFields;
	private BindParameterType[] keyFieldTypes;
	private String[] fields;
	@SuppressWarnings("unused")
	private BindParameterType[] fieldTypes;
	TableInfo tableInfo = null;
	private DBContext dbContext;
	private HashMap<String, Object> internalStore = new HashMap<String, Object>();
	StringBuffer sqlQuery = new StringBuffer();
	boolean create = true;
	DTObject input = new DTObject();
	private String operationType;

	public HashMap<String, Object> getInternalStore() {
		return internalStore;
	}

	public RecordConfiguration() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("()");
	}

	public RecordConfiguration(String tableName, DBContext dbContext) {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("()");
		this.tableName = tableName;
		this.dbContext = dbContext;
		init();
	}

	private void init() {
		logger.logDebug("init() - Begin");
		setTableName(tableName);
		DBInfo info = new DBInfo();
		tableInfo = info.getTableInfo(this.tableName);
		fields = tableInfo.getColumnInfo().getColumnNames();
		fieldTypes = tableInfo.getColumnInfo().getColumnTypes();
		keyFields = tableInfo.getPrimaryKeyInfo().getColumnNames();
		keyFieldTypes = tableInfo.getPrimaryKeyInfo().getColumnTypes();
		logger.logDebug("init() - End");
	}

	private void subClose() {
		internalStore.clear();
		keyFields = null;
		keyFieldTypes = null;
		tableInfo = null;
	}

	public void close() {
		subClose();
	}

	private void setTableName(String tableName) {
		subClose();
		this.tableName = tableName;
	}

	public String getTableName() {
		return tableName;
	}

	public void setField(String fieldName, Object fieldValue) {
		internalStore.put(fieldName, fieldValue);
	}

	public Object getField(String fieldName) {
		return internalStore.get(fieldName);
	}

	public int deleteByKeyFields(List<Map<String, Object>> recordList) {
		/* CHANGED BY PAVAN ON 17032015 */
		return deleteByFields(keyFields, keyFieldTypes, recordList.get(0));
	}

	public String getDate(Date date, String format) {
		if (date == null)
			return null;
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String result = null;
		result = sdf.format(date);
		return result;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	private String getTBAPrimaryKey() {
		StringBuffer primaryKey = new StringBuffer("");
		int i = 0;
		for (BindParameterType fieldType : keyFieldTypes) {
			Object value = null;
			switch (fieldType) {
			case VARCHAR:
				primaryKey = primaryKey.append((String) getField(keyFields[i])).append("|");
				break;
			case DATE:
				value = getField(keyFields[i]);
				if (value instanceof java.util.Date)
					primaryKey = primaryKey.append(BackOfficeFormatUtils.getDate(((java.util.Date) value), BackOfficeConstants.TBA_DATE_FORMAT)).append("|");
				break;
			case INTEGER:
				value = getField(keyFields[i]);
				Integer number = Integer.parseInt((String) value);
				primaryKey = primaryKey.append(number.toString()).append("|");
				break;
			case DECIMAL:
				value = getField(keyFields[i]);
				BigDecimal decimal = new BigDecimal((String) value);
				primaryKey = primaryKey.append(decimal.toString()).append("|");
				break;
			case BIGDECIMAL:
				break;
			case BIGINT:
				value = getField(keyFields[i]);
				BigInteger integer = new BigInteger((String) value);
				primaryKey = primaryKey.append(integer.toString()).append("|");
				break;
			case CUSTOM:
				break;
			case LONG:
				break;
			case TIMESTAMP:
				break;
			default:
				break;
			}
			i++;
		}
		if (primaryKey.length() > 1) {
			return primaryKey.toString().substring(0, primaryKey.toString().length() - 1);
		}
		return primaryKey.toString();
	}

	private String getDtlTablePrimaryKey(Map<String, Object> record) {
		StringBuffer primaryKey = new StringBuffer("");
		int i = 0;
		int keyFieldCount = (keyFieldTypes.length - 1);
		for (BindParameterType fieldType : keyFieldTypes) {
			if (i == keyFieldCount)
				break;
			Object value = null;
			switch (fieldType) {
			case VARCHAR:
				primaryKey = primaryKey.append((String) record.get(keyFields[i])).append("|");
				break;
			case DATE:
				value = record.get(keyFields[i]);
				if (value instanceof java.util.Date)
					primaryKey = primaryKey.append(BackOfficeFormatUtils.getDate(((java.util.Date) value), BackOfficeConstants.TBA_DATE_FORMAT)).append("|");
				break;
			case INTEGER:
				value = record.get(keyFields[i]);
				Integer number = Integer.parseInt((String) value);
				primaryKey = primaryKey.append(number.toString()).append("|");
				break;
			case DECIMAL:
				value = record.get(keyFields[i]);
				BigDecimal decimal = new BigDecimal((String) value);
				primaryKey = primaryKey.append(decimal.toString()).append("|");
				break;
			case BIGDECIMAL:
				break;
			case BIGINT:
				value = record.get(keyFields[i]);
				BigInteger integer = new BigInteger((String) value);
				primaryKey = primaryKey.append(integer.toString()).append("|");
				break;
			case CUSTOM:
				break;
			case LONG:
				break;
			case TIMESTAMP:
				break;
			default:
				break;
			}
			++i;
		}
		if (primaryKey.length() > 1) {
			return primaryKey.toString().substring(0, primaryKey.toString().length() - 1);
		}
		return primaryKey.toString();
	}

	public int deleteByFields(String[] keyFields, BindParameterType[] keyFieldTypes, Map<String, Object> record) {
		int count = 0;
		sqlQuery.setLength(0);
		sqlQuery.append("DELETE FROM ").append(tableName);
		if (keyFields.length > 0)
			sqlQuery.append(" WHERE ");
		int i = 0;
		for (String fieldName : keyFields) {
			if (i < keyFields.length - 2) {
				sqlQuery.append(fieldName).append(" = ? AND ");
			} else {
				sqlQuery.append(fieldName).append(" = ?");
				break;
			}
			i++;
		}
		sqlQuery.trimToSize();
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			i = 0;
			int keyFieldCount = keyFieldTypes.length;
			for (BindParameterType fieldType : keyFieldTypes) {
				++i;
				if (i == keyFieldCount)
					break;
				switch (fieldType) {
				case LONG:
					util.setLong(i, (Long) record.get(keyFields[i - 1]));
					break;
				case VARCHAR:
					Object value1 = record.get(keyFields[i - 1]);
					if (value1 instanceof String)
						util.setString(i, (String) value1);
					else if (value1 instanceof Integer)
						util.setInt(i, Integer.parseInt(String.valueOf(value1)));
					break;
				case DATE:
					Object value = record.get(keyFields[i - 1]);
					if (value == null)
						util.setNull(i, Types.DATE);
					else if (value instanceof Timestamp)
						util.setTimestamp(i, (Timestamp) value);
					else if (value instanceof java.sql.Date)
						util.setDate(i, (Date) value);
					else if (value instanceof java.util.Date)
						util.setDate(i, new java.sql.Date(((java.util.Date) value).getTime()));
					break;
				case INTEGER:
					/* CHANGED BY PAVAN ON 17032015 BEG */
					Object integerValue = record.get(keyFields[i - 1]);
					if (integerValue instanceof String)
						util.setString(i, (String) integerValue);
					else if (integerValue instanceof Integer)
						util.setInt(i, (Integer) integerValue);
					break;
				/* CHANGED BY PAVAN ON 17032015 END */
				case TIMESTAMP:
					Object dateValue = record.get(keyFields[i - 1]);
					if (dateValue instanceof Timestamp)
						util.setTimestamp(i, (Timestamp) dateValue);
					else
						util.setDate(i, new java.sql.Date(((java.util.Date) dateValue).getTime()));
					break;
				case DECIMAL:
					util.setBigDecimal(i, (BigDecimal) record.get(keyFields[i - 1]));
					break;
				case BIGINT:
					Object _value = record.get(keyFields[i - 1]);
					if (_value instanceof Long)
						util.setLong(i, (Long) _value);
					else
						util.setString(i, (String) _value);
					break;
				case BIGDECIMAL:
					break;
				case CUSTOM:
					break;
				default:
					break;
				}
			}
			count = util.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			count = 0;
		} finally {
			util.reset();
		}
		return count;
	}

	public void setNew(boolean create) {
		this.create = create;
	}

	public boolean isNew() {
		return create;
	}

	public boolean save() throws JobsException {
		logger.logDebug("save(1) - Begin");
		boolean saved = false;
		if (isNew()) {
			saved = insert();
		} else {
			saved = update();
		}

		if (getField("CR_BY") != null || getField("AU_BY") != null || getField("MO_BY") != null) {
			updateAuditOperations();
		}
		/* CHANGED BY PAVAN ON 28052015 BEG */
		if (getField("MCR_BY") != null || getField("MAU_BY") != null || getField("MMO_BY") != null) {
			MasterUpdateAuditOperations();
		}
		/* CHANGED BY PAVAN ON 28052015 END */

		logger.logDebug("save(1) - End");
		return saved;
	}

	/* CHANGED BY PAVAN ON 28052015 BEG */
	private void MasterUpdateAuditOperations() throws JobsException {
		logger.logDebug("MasterUpdateAuditOperations() - Begin");
		DBUtil dbUtil = dbContext.createUtilInstance();
		try {
			dbUtil.reset();
			dbUtil.setMode(DBUtil.CALLABLE);
			dbUtil.setSql("CALL SP_CTJUPDATE_MASTERAUDITFIELDS(?,?,?,?,?,?,?,?,?,?,?,?)");
			dbUtil.setString(1, (String) getField("ENTITY_CODE"));
			dbUtil.setString(2, (String) getField("MCR_BY"));
			Object value = getField("MCR_ON");
			if (value == null)
				dbUtil.setNull(3, Types.DATE);
			else if (value instanceof java.util.Date)
				dbUtil.setDate(3, new java.sql.Date(((java.util.Date) value).getTime()));
			dbUtil.setString(4, (String) getField("MMO_BY"));
			value = getField("MMO_ON");
			if (value == null)
				dbUtil.setNull(5, Types.DATE);
			else if (value instanceof java.util.Date)
				dbUtil.setDate(5, new java.sql.Date(((java.util.Date) value).getTime()));
			dbUtil.setString(6, (String) getField("MAU_BY"));
			value = getField("MAU_ON");
			if (value == null)
				dbUtil.setNull(7, Types.DATE);
			else if (value instanceof java.util.Date)
				dbUtil.setDate(7, new java.sql.Date(((java.util.Date) value).getTime()));
			dbUtil.setString(8, (String) getField("PROGRAM_ID"));
			dbUtil.setString(9, this.tableName);
			String pk = getTBAPrimaryKey();
			dbUtil.setString(10, pk);
			if (isNew()) {
				dbUtil.setString(11, "W");
			} else {
				dbUtil.setString(11, "R");
			}
			dbUtil.registerOutParameter(12, Types.VARCHAR);
			dbUtil.execute();
			dbUtil.getString(12);
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError("Error1 - " + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.DATABASE_UPDATE_E);
		} finally {
			dbUtil.reset();
		}
		logger.logDebug("MasterupdateAuditOperations() - End");
	}

	/* ADDED ON 10072015 BY PAVAN BEGIN */
	public void MasterUpdateAuditOperations(DTObject input) throws JobsException {
		logger.logDebug("MasterUpdateAuditOperations(DTObject input) - Begin");
		DBUtil dbUtil = (DBUtil) input.getObject("DBUTIL");
		try {
			dbUtil.reset();
			dbUtil.setMode(DBUtil.CALLABLE);
			dbUtil.setSql("CALL SP_CTJUPDATE_MASTERAUDITFIELDS(?,?,?,?,?,?,?,?,?,?,?,?)");
			dbUtil.setString(1, input.get("ENTITY_CODE"));
			dbUtil.setString(2, input.get("CR_BY"));
			Object value = input.getObject("CR_ON");
			if (value == null)
				dbUtil.setNull(3, Types.DATE);
			else if (value instanceof java.util.Date)
				dbUtil.setDate(3, new java.sql.Date(((java.util.Date) value).getTime()));
			dbUtil.setString(4, input.get("MO_BY"));
			value = input.getObject("MO_ON");
			if (value == null)
				dbUtil.setNull(5, Types.DATE);
			else if (value instanceof java.util.Date)
				dbUtil.setDate(5, new java.sql.Date(((java.util.Date) value).getTime()));
			dbUtil.setString(6, input.get("AU_BY"));
			value = input.getObject("AU_ON");
			if (value == null)
				dbUtil.setNull(7, Types.DATE);
			else if (value instanceof java.util.Date)
				dbUtil.setDate(7, new java.sql.Date(((java.util.Date) value).getTime()));
			dbUtil.setString(8, input.get("PROGRAM_ID"));
			dbUtil.setString(9, input.get("TABLE"));
			dbUtil.setString(10, input.get("PK"));
			dbUtil.setString(11, input.get("OPERATION"));
			dbUtil.registerOutParameter(12, Types.VARCHAR);
			dbUtil.execute();
			dbUtil.getString(12);
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError("Error1 - " + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.DATABASE_UPDATE_E);
		} finally {
			dbUtil.reset();
		}
		logger.logDebug("updateAuditOperations() - End");
	}
	/* ADDED ON 10072015 BY PAVAN END */
	
	/* CHANGED BY PAVAN ON 28052015 END */
	public boolean save(List<Map<String, Object>> recordList) throws JobsException {
		logger.logDebug("save(2) - Begin");
		boolean saved = false;
		saved = insert(recordList);
		/* CHANGED BY PAVAN ON 17032015 BEG */
		Map<String, Object> record = recordList.get(0);
		/* CHANGED BY PAVAN ON 17032015 END */
		String primaryKey = getDtlTablePrimaryKey(record);

		if (record.get("CR_BY") != null || record.get("AU_BY") != null || record.get("MO_BY") != null) {
			DTObject input = new DTObject();
			input.set("ENTITY_CODE", (String) record.get("ENTITY_CODE"));
			input.set("CR_BY", (String) record.get("CR_BY"));
			input.setObject("CR_ON", record.get("CR_ON"));
			input.set("MO_BY", (String) record.get("MO_BY"));
			input.setObject("MO_ON", record.get("MO_ON"));
			input.set("AU_BY", (String) record.get("AU_BY"));
			input.setObject("AU_ON", record.get("AU_ON"));
			input.set("PROGRAM_ID", (String) record.get("PROGRAM_ID"));
			input.set("TABLE", this.tableName);
			input.set("OPERATION", operationType);
			input.set("PK", primaryKey);
			input.setObject("DBUTIL", dbContext.createUtilInstance());
			updateAuditOperations(input);
		}
		logger.logDebug("save(2) - End");
		return saved;
	}

	private void updateAuditOperations() throws JobsException {
		logger.logDebug("updateAuditOperations() - Begin");
		DBUtil dbUtil = dbContext.createUtilInstance();
		try {
			dbUtil.reset();
			dbUtil.setMode(DBUtil.CALLABLE);
			dbUtil.setSql("CALL SP_CTJUPDATE_AUDITFIELDS(?,?,?,?,?,?,?,?,?,?,?,?)");
			dbUtil.setString(1, (String) getField("ENTITY_CODE"));
			dbUtil.setString(2, (String) getField("CR_BY"));
			Object value = getField("CR_ON");
			if (value == null)
				dbUtil.setNull(3, Types.DATE);
			else if (value instanceof java.util.Date)
				dbUtil.setDate(3, new java.sql.Date(((java.util.Date) value).getTime()));
			dbUtil.setString(4, (String) getField("MO_BY"));
			value = getField("MO_ON");
			if (value == null)
				dbUtil.setNull(5, Types.DATE);
			else if (value instanceof java.util.Date)
				dbUtil.setDate(5, new java.sql.Date(((java.util.Date) value).getTime()));
			dbUtil.setString(6, (String) getField("AU_BY"));
			value = getField("AU_ON");
			if (value == null)
				dbUtil.setNull(7, Types.DATE);
			else if (value instanceof java.util.Date)
				dbUtil.setDate(7, new java.sql.Date(((java.util.Date) value).getTime()));
			dbUtil.setString(8, (String) getField("PROGRAM_ID"));
			dbUtil.setString(9, this.tableName);
			String pk = getTBAPrimaryKey();
			dbUtil.setString(10, pk);
			if (isNew()) {
				dbUtil.setString(11, "W");
			} else {
				dbUtil.setString(11, "R");
			}
			dbUtil.registerOutParameter(12, Types.VARCHAR);
			dbUtil.execute();
			dbUtil.getString(12);
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError("Error1 - " + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.DATABASE_UPDATE_E);
		} finally {
			dbUtil.reset();
		}
		logger.logDebug("updateAuditOperations() - End");
	}
	
	public void updateAuditOperations(DTObject input) throws JobsException {
		logger.logDebug("updateAuditOperations() - Begin");
		DBUtil dbUtil = (DBUtil) input.getObject("DBUTIL");
		try {
			dbUtil.reset();
			dbUtil.setMode(DBUtil.CALLABLE);
			dbUtil.setSql("CALL SP_CTJUPDATE_AUDITFIELDS(?,?,?,?,?,?,?,?,?,?,?,?)");
			dbUtil.setString(1, input.get("ENTITY_CODE"));
			dbUtil.setString(2, input.get("CR_BY"));
			Object value = input.getObject("CR_ON");
			if (value == null)
				dbUtil.setNull(3, Types.DATE);
			else if (value instanceof java.util.Date)
				dbUtil.setDate(3, new java.sql.Date(((java.util.Date) value).getTime()));
			dbUtil.setString(4, input.get("MO_BY"));
			value = input.getObject("MO_ON");
			if (value == null)
				dbUtil.setNull(5, Types.DATE);
			else if (value instanceof java.util.Date)
				dbUtil.setDate(5, new java.sql.Date(((java.util.Date) value).getTime()));
			dbUtil.setString(6, input.get("AU_BY"));
			value = input.getObject("AU_ON");
			if (value == null)
				dbUtil.setNull(7, Types.DATE);
			else if (value instanceof java.util.Date)
				dbUtil.setDate(7, new java.sql.Date(((java.util.Date) value).getTime()));
			dbUtil.setString(8, input.get("PROGRAM_ID"));
			dbUtil.setString(9, input.get("TABLE"));
			dbUtil.setString(10, input.get("PK"));
			dbUtil.setString(11, input.get("OPERATION"));
			dbUtil.registerOutParameter(12, Types.VARCHAR);
			dbUtil.execute();
			dbUtil.getString(12);
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError("Error1 - " + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.DATABASE_UPDATE_E);
		} finally {
			dbUtil.reset();
		}
		logger.logDebug("updateAuditOperations() - End");
	}

	private boolean update() throws JobsException {
		logger.logDebug("update()-Begin");
		sqlQuery = new StringBuffer();
		boolean saved = false;
		Set<String> noUpdationTableInfo = tableInfo.getNoUpdationColumnInfo();
		sqlQuery.append("UPDATE ").append(tableName).append(" SET ");
		for (String fieldName : fields) {
			if (!noUpdationTableInfo.contains(fieldName)) {
				if (internalStore.containsKey(fieldName)) {
					sqlQuery.append(fieldName).append(" = ? ,");
				}
			}
		}
		sqlQuery.trimToSize();
		sqlQuery = new StringBuffer(sqlQuery.substring(0, sqlQuery.length() - 1));
		if (keyFields.length > 0)
			sqlQuery.append(" WHERE ");
		int i = 0;
		for (String fieldName : keyFields) {
			if (i < keyFields.length - 1) {
				sqlQuery.append(fieldName).append(" = ? AND ");
			} else {
				sqlQuery.append(fieldName).append(" = ?");
			}
			++i;
		}
		int count = 0;
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			i = 0;
			Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
			for (String fieldName : fields) {
				if (!noUpdationTableInfo.contains(fieldName)) {
					if (internalStore.containsKey(fieldName)) {
						++i;
						Object value = null;
						switch (columnType.get(fieldName)) {
						case LONG:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.NUMERIC);
							else if (value instanceof Long)
								util.setLong(i, (Long) value);
							else
								util.setString(i, (String) value);
							break;
						case VARCHAR:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.VARCHAR);
							else
								util.setString(i, (String) value);
							break;
						case DATE:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.DATE);
							else if (value instanceof Timestamp)
								util.setTimestamp(i, (Timestamp) value);
							else if (value instanceof java.sql.Date)
								util.setDate(i, (Date) value);
							else if (value instanceof java.util.Date)
								util.setDate(i, new java.sql.Date(((java.util.Date) value).getTime()));
							else
								util.setString(i, (String) value);
							break;
						case INTEGER:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.INTEGER);
							else if (value instanceof Integer)
								util.setInt(i, (Integer) value);
							else if (value instanceof BigDecimal)
								util.setBigDecimal(i, (BigDecimal) value);
							else
								util.setString(i, (String) value);
							break;
						case TIMESTAMP:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.TIMESTAMP);
							else if (value instanceof Timestamp)
								util.setTimestamp(i, (Timestamp) value);
							else if (value instanceof java.util.Date)
								util.setDate(i, new java.sql.Date(((java.util.Date) value).getTime()));
							else
								util.setString(i, (String) value);
							break;
						case DECIMAL:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.DECIMAL);
							else if (value instanceof BigDecimal)
								util.setBigDecimal(i, (BigDecimal) value);
							else
								util.setString(i, (String) value);
							break;
						case BIGINT:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.DECIMAL);
							else if (value instanceof Long)
								util.setLong(i, (Long) value);
							else
								util.setString(i, (String) value);
							break;
						case BIGDECIMAL:
							break;
						case CUSTOM:
							break;
						default:
							break;
						}
					}
				}
			}
			int k = 0;
			++i;
			for (BindParameterType fieldType : keyFieldTypes) {
				Object value = null;
				switch (fieldType) {
				case LONG:
					value = getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.NUMERIC);
					else if (value instanceof Long)
						util.setLong(i, (Long) value);
					else
						util.setString(i, (String) value);
					break;
				case VARCHAR:
					value = (String) getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.VARCHAR);
					else
						util.setString(i, (String) value);
					break;
				case DATE:
					value = getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.DATE);
					else if (value instanceof Timestamp)
						util.setTimestamp(i, (Timestamp) value);
					else if (value instanceof java.sql.Date)
						util.setDate(i, (Date) value);
					else if (value instanceof java.util.Date)
						util.setDate(i, new java.sql.Date(((java.util.Date) getField(keyFields[k])).getTime()));
					else
						util.setString(i, (String) value);
					break;
				case INTEGER:
					value = getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.INTEGER);
					else if (value instanceof Integer)
						util.setInt(i, (Integer) value);
					else if (value instanceof BigDecimal)
						util.setBigDecimal(i, (BigDecimal) value);
					else
						util.setString(i, (String) value);
					break;
				case TIMESTAMP:
					value = getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.TIMESTAMP);
					else if (value instanceof Timestamp)
						util.setTimestamp(i, (Timestamp) value);
					else if (value instanceof java.util.Date)
						util.setDate(i, new java.sql.Date(((java.util.Date) value).getTime()));
					else
						util.setString(i, (String) value);
					break;
				case DECIMAL:
					value = getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.DECIMAL);
					else if (value instanceof BigDecimal)
						util.setBigDecimal(i, (BigDecimal) value);
					else
						util.setString(i, (String) value);
					break;
				case BIGINT:
					value = getField(keyFields[k]);
					if (value == null)
						util.setNull(i, Types.DECIMAL);
					else if (value instanceof Long)
						util.setLong(i, (Long) value);
					else
						util.setString(i, (String) value);
					break;
				case BIGDECIMAL:
					break;
				case CUSTOM:
					break;
				default:
					break;
				}
				++k;
				++i;
			}
			count = util.executeUpdate();
			if (count == 0)
				logger.logDebug("update" + tableName + "count==0");
			if (count > 0)
				saved = true;
		} catch (SQLException e) {
			logger.logError("Error - " + e.getLocalizedMessage());
			e.printStackTrace();
			throw new JobsException(JobsErrorCodes.DATABASE_UPDATE_E);
		} finally {
			util.reset();
		}
		logger.logDebug("update()-End");
		return saved;
	}

	public boolean insert() throws JobsException {
		logger.logDebug("insert(1)-Begin");
		sqlQuery = new StringBuffer();
		Set<String> noUpdationTableInfo = tableInfo.getNoUpdationColumnInfo();
		StringBuffer buffer = new StringBuffer();
		boolean saved = false;
		sqlQuery.append("INSERT INTO ").append(tableName).append(" ( ");
		for (String fieldName : fields) {
			if (!noUpdationTableInfo.contains(fieldName)) {
				if (internalStore.containsKey(fieldName)) {
					sqlQuery.append(fieldName).append(",");
					buffer.append("?").append(",");
				}
			}
		}
		buffer.trimToSize();
		buffer = new StringBuffer(buffer.substring(0, buffer.length() - 1));
		sqlQuery.trimToSize();
		sqlQuery = new StringBuffer(sqlQuery.substring(0, sqlQuery.length() - 1));
		sqlQuery.append(") VALUES(");
		sqlQuery.append(buffer.toString());
		sqlQuery.append(")");

		int count = 0;
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			int i = 0;
			Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
			for (String fieldName : fields) {
				if (!noUpdationTableInfo.contains(fieldName)) {
					if (internalStore.containsKey(fieldName)) {
						++i;
						Object value = null;
						switch (columnType.get(fieldName)) {
						case LONG:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.NUMERIC);
							else if (value instanceof Long)
								util.setLong(i, (Long) value);
							else
								util.setString(i, (String) value);
							break;
						case VARCHAR:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.VARCHAR);
							else
								util.setString(i, (String) value);
							break;
						case DATE:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.DATE);
							else if (value instanceof Timestamp)
								util.setTimestamp(i, (Timestamp) value);
							else if (value instanceof java.sql.Date)
								util.setDate(i, (Date) value);
							else if (value instanceof java.util.Date)
								util.setDate(i, new java.sql.Date(((java.util.Date) getField(fieldName)).getTime()));
							else
								util.setString(i, (String) value);
							break;
						case INTEGER:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.INTEGER);
							else if (value instanceof Integer)
								util.setInt(i, (Integer) value);
							else if (value instanceof BigDecimal)
								util.setBigDecimal(i, (BigDecimal) value);
							else
								util.setString(i, (String) value);
							break;
						case TIMESTAMP:
							/* CHANGED BY PAVAN ON 17032015 BEG */
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.TIMESTAMP);
							else if (value instanceof Timestamp)
								util.setTimestamp(i, (Timestamp) value);
							else if (value instanceof java.util.Date)
								util.setDate(i, new java.sql.Date(((java.util.Date) value).getTime()));
							else
								util.setString(i, (String) value);
							break;
						/* CHANGED BY PAVAN ON 17032015 END */
						case DECIMAL:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.DECIMAL);
							else if (value instanceof BigDecimal)
								util.setBigDecimal(i, (BigDecimal) value);
							else
								util.setString(i, (String) value);
							break;
						case BIGINT:
							value = getField(fieldName);
							if (value == null)
								util.setNull(i, Types.DECIMAL);
							else if (value instanceof Long)
								util.setLong(i, (Long) value);
							else
								util.setString(i, (String) value);
							break;
						case BIGDECIMAL:
							break;
						case CUSTOM:
							break;
						default:
							break;
						}
					}
				}
			}
			count = util.executeUpdate();
			if (count > 0)
				saved = true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError("Error - " + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.DATABASE_UPDATE_E);
		} finally {
			util.reset();
		}
		logger.logDebug("insert(1)-End");
		return saved;
	}

	public boolean insert(List<Map<String, Object>> input) throws JobsException {
		logger.logDebug("insert(2)-Begin");
		sqlQuery = new StringBuffer();
		StringBuffer buffer = new StringBuffer();
		boolean saved = false;
		sqlQuery.append("INSERT INTO ").append(tableName).append(" ( ");
		for (String fieldName : fields) {
			sqlQuery.append(fieldName).append(",");
			buffer.append("?").append(",");
		}
		buffer.trimToSize();
		buffer = new StringBuffer(buffer.substring(0, buffer.length() - 1));
		sqlQuery.trimToSize();
		sqlQuery = new StringBuffer(sqlQuery.substring(0, sqlQuery.length() - 1));
		sqlQuery.append(") VALUES(");
		sqlQuery.append(buffer.toString());
		sqlQuery.append(")");

		for (int i = 1; i < input.size(); i++) {
			sqlQuery.append(",(").append(buffer.toString()).append(")");
		}

		int count = 0;
		DBUtil util = dbContext.createUtilInstance();
		try {
			util.reset();
			util.setSql(sqlQuery.toString());
			int i = 0;
			for (Map<String, Object> record : input) {
				Map<String, BindParameterType> columnType = tableInfo.getColumnInfo().getColumnMapping();
				for (String fieldName : fields) {
					++i;
					Object value = null;
					switch (columnType.get(fieldName)) {
					case LONG:
						value = record.get(fieldName);
						if (value == null)
							util.setNull(i, Types.NUMERIC);
						else if (value instanceof Long)
							util.setLong(i, (Long) value);
						else
							util.setString(i, (String) value);
						break;
					case VARCHAR:
						value = record.get(fieldName);
						if (value == null)
							util.setNull(i, Types.VARCHAR);
						else
							util.setString(i, (String) value);
						break;
					case DATE:
						value = record.get(fieldName);
						if (value == null)
							util.setNull(i, Types.DATE);
						else if (value instanceof Timestamp)
							util.setTimestamp(i, (Timestamp) value);
						else if (value instanceof java.sql.Date)
							util.setDate(i, (Date) value);
						else if (value instanceof java.util.Date)
							util.setDate(i, new java.sql.Date(((java.util.Date) value).getTime()));
						else
							util.setString(i, (String) value);
						break;
					case INTEGER:
						value = record.get(fieldName);
						if (value == null)
							util.setNull(i, Types.INTEGER);
						else if (value instanceof Integer)
							util.setInt(i, (Integer) value);
						else if (value instanceof BigDecimal)
							util.setBigDecimal(i, (BigDecimal) value);
						else
							util.setString(i, (String) value);
						break;
					case TIMESTAMP:
						/* CHANGED BY PAVAN ON 17032015 BEG */
						value = record.get(fieldName);
						if (value == null)
							util.setNull(i, Types.TIMESTAMP);
						else if (value instanceof Timestamp)
							util.setTimestamp(i, (Timestamp) value);
						else if (value instanceof java.util.Date)
							util.setDate(i, new java.sql.Date(((java.util.Date) value).getTime()));
						else
							util.setString(i, (String) value);
						break;
					/* CHANGED BY PAVAN ON 17032015 END */
					case DECIMAL:
						value = record.get(fieldName);
						if (value == null)
							util.setNull(i, Types.DECIMAL);
						else if (value instanceof BigDecimal)
							util.setBigDecimal(i, (BigDecimal) value);
						else
							util.setString(i, (String) value);
						break;
					case BIGINT:
						value = record.get(fieldName);
						if (value == null)
							util.setNull(i, Types.DECIMAL);
						else if (value instanceof Long)
							util.setLong(i, (Long) value);
						else
							util.setString(i, (String) value);
						break;
					case BIGDECIMAL:
						break;
					case CUSTOM:
						break;
					default:
						break;
					}
				}
			}
			count = util.executeUpdate();
			if(count==0)
				logger.logDebug("update" + tableName + " count==0");
			if (count > 0)
				saved = true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.logError("Error - " + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.DATABASE_UPDATE_E);
		} finally {
			util.reset();
		}
		logger.logDebug("insert(2)-End");
		return saved;
	}

	public Date getCurrentDate(String entityCode) throws JobsException {
		logger.logDebug("getCurrentDate() - Begin");
		Date systemDate = null;
		ResultSet rs = null;
		DBUtil dbutil = dbContext.createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT FN_GETCD(?) FROM DUAL");
			dbutil.setString(1, entityCode);
			rs = dbutil.executeQuery();
			if (rs.next()) {
				systemDate = rs.getDate(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			logger.logError("Error - " + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.DATABASE_UPDATE_E);
		} finally {
			dbutil.reset();
		}
		logger.logDebug("getCurrentDate()-End");
		return systemDate;
	}

}
