package com.patterns.jobs.message.handler;

import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.quartz.JobExecutionException;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;

import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public abstract class AbstractMessageHandler {
	private final String RECORD_START_DELIMITER = "{";
	private final String RECORD_END_DELIMITER = "}";

	protected final ApplicationLogger logger;

	private DTObject processContext;
	private List<String> recordList = new LinkedList<String>();
	private DBContext dbContext;
	private String processingCode;
	private String entityCode;
	private String programId;

	public DTObject getProcessContext() {
		return processContext;
	}

	public List<String> getRecordList() {
		return recordList;
	}

	public DBContext getDbContext() {
		return dbContext;
	}

	public String getProcessingCode() {
		return processingCode;
	}

	public String getEntityCode() {
		return entityCode;
	}

	public String getProgramId() {
		return programId;
	}

	public AbstractMessageHandler() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("()");
	}

	public void execute(DTObject context) throws JobsException {
		logger.logDebug("execute() Begin");
		try {
			this.processContext = context;
			this.processingCode = context.get("PROCESSING_CODE");
			this.entityCode = context.get("ENTITY_CODE");
			this.programId = context.get("PROGRAM_ID");
			this.dbContext = (DBContext) context.getObject("DBCONTEXT");

			readRecords();

			process();
		} catch (Exception e) {
			logger.logError("execute() Exception" + e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.MSG_HANDLER_PROCCESSING_FAIL_E);
		}
		logger.logDebug("execute() End");
	}

	private void readRecords() throws JobsException {
		logger.logDebug("readRecords() - Begin");
		StringBuffer dataBlock = new StringBuffer("");
		try {
			String readMessageBlockSql = "SELECT MSG_DATA_CONTENT FROM CTJBLOCK WHERE ENTITY_CODE = ? AND MSG_REF_ID = ?";
			DBUtil readMessageBlockUtil = dbContext.createUtilInstance();
			readMessageBlockUtil.setSql(readMessageBlockSql);
			readMessageBlockUtil.setString(1, getProcessContext().get("ENTITY_CODE"));
			readMessageBlockUtil.setString(2, getProcessContext().get("REF_NUMBER"));
			ResultSet readMessageBlockRset = readMessageBlockUtil.executeQuery();
			while (readMessageBlockRset.next()) {
				dataBlock.append(readMessageBlockRset.getString("MSG_DATA_CONTENT"));
			}
			readMessageBlockUtil.reset();

			Pattern p = Pattern.compile("\\" + RECORD_START_DELIMITER + "(.*?)\\" + RECORD_END_DELIMITER);
			Matcher m = p.matcher(dataBlock.toString());
			while (m.find()) {
				getRecordList().add(m.group().substring(1, m.group().length() - 1));
			}
		} catch (Exception e) {
			logger.logError(e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.MSG_HANDLER_PROCCESSING_FAIL_E);
		}
		logger.logDebug("readRecords() - End");
	}

	public abstract void process() throws JobExecutionException, JobsException;

}
