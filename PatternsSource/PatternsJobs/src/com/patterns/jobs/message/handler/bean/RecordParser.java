package com.patterns.jobs.message.handler.bean;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;
import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.web.BackOfficeFormatUtils;

import com.patterns.interfaces.config.manager.KeyConfigManager;
import com.patterns.interfaces.config.manager.bean.KeyConfigBean;
import com.patterns.interfaces.config.manager.bean.MessageConfigDetailBean;
import com.patterns.interfaces.config.manager.bean.PreProcessorKeyConfigBean;
import com.patterns.jobs.exceptions.JobsErrorCodes;
import com.patterns.jobs.exceptions.JobsException;

public class RecordParser {
	private final int FILENAME_START_INDEX = 0;
	private final int FILENAME_END_INDEX = 40;
	private final int FILETYPE_START_INDEX = 40;
	private final int FILETYPE_END_INDEX = 50;
	private final int RECORDOPR_START_INDEX = 50;
	private final int RECORDOPR_END_INDEX = 51;
	private final int RECORDDATA_START_INDEX = 51;
	public static final String RECORD_COLUMN_DELIMITER = "#";

	private ApplicationLogger logger = null;

	private String recordContent;
	private String entityCode;
	private String processingCode;

	private String tableType;
	private String tableName;
	private String operationType;
	private DBContext dbContext;

	public String getTableName() {
		return tableName;
	}

	public String getTableType() {
		return tableType;
	}

	public String getOperationType() {
		return operationType;
	}

	private final String VALUEDATATYPE_STRING = "1";
	private final String VALUEDATATYPE_NUMBER = "2";
	private final String VALUEDATATYPE_DECIMAL = "3";
	private final String VALUEDATATYPE_DATE = "4";

	public RecordParser(DBContext dbContext, String recordContent, String entityCode, String processingCode) {
		this.recordContent = recordContent;
		this.entityCode = entityCode;
		this.processingCode = processingCode;
		this.dbContext = dbContext;
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Map<String, Object> parseRecordDetails() throws JobsException {
		logger.logDebug("parseRecordDetails() - Begin");
		Map<String, Object> recordDetails = null;
		try {
			String fileName = recordContent.substring(FILENAME_START_INDEX, FILENAME_END_INDEX).trim();
			String fileType = recordContent.substring(FILETYPE_START_INDEX, FILETYPE_END_INDEX).trim();
			String recordOperation = recordContent.substring(RECORDOPR_START_INDEX, RECORDOPR_END_INDEX).trim();
			String data = recordContent.substring(RECORDDATA_START_INDEX);

			MessageConfigDetailBean configurationBean = KeyConfigManager.getInstance().getMessageConfiguration(
					entityCode, processingCode, fileName, fileType);
			String[] dataList = data.split("#");

			System.out.println("DATA : " + data);

			if (configurationBean == null) {
				logger.logError("CONFIGURATION UNAVAILABLE FOR - " + fileName + " - " + fileType + " - "
						+ recordOperation);
				throw new JobsException(JobsErrorCodes.CFG_UNAVAIL_E);
			}

			tableType = configurationBean.getTableType();

			recordDetails = new HashMap<String, Object>();
			Map<String, Object> input = new HashMap<String, Object>();
			if (configurationBean.isPreprocessReqd()) {
				List<PreProcessorKeyConfigBean> preProcessorkeyConfigurationList = KeyConfigManager.getInstance()
						.getPreProcessorTableConfiguration(entityCode, processingCode, fileName, fileType);
				input.put("ENTITY_CODE", entityCode);
				input.put("RECORD", dataList);
				input.put("DBCONTEXT", dbContext);
				input.put("OPERATION", recordOperation);
				for (PreProcessorKeyConfigBean bean : preProcessorkeyConfigurationList) {
					input.put("CONFIG_DETAILS", bean.getConfigurationDetailsMap());
					input.put("TABLE_NAME", bean.getTableName());
					input.put("COLUMN_NAME", bean.getColumnName());

					Class classObject = null;
					Object object = null;
					Method methodObject = null;
					Class[] parameterTypes = new Class[] { Map.class };
					Object[] params = new Object[] { input };
					classObject = Class.forName(bean.getHandlerName());
					object = classObject.newInstance();
					methodObject = classObject.getMethod("execute", parameterTypes);
					input = (Map) methodObject.invoke(object, params);
				}
			}

			recordDetails.putAll(input);

			List<KeyConfigBean> keyConfigurationList = KeyConfigManager.getInstance().getTableConfiguration(entityCode,
					processingCode, fileName, fileType, recordOperation);

			for (KeyConfigBean bean : keyConfigurationList) {
				tableName = bean.getTableName();
				int index = bean.getIndex();

				String value;
				if (index == -1) {
					value = entityCode;
				} else if (index == 999) {
					value = bean.getDefaultValue();
				} else {
					value = dataList[index];
					if (bean.getTrimLength() > 0) {
						if (value.length() > bean.getTrimLength()) {
							value = value.substring(0, bean.getTrimLength());
						}
					}
				}

				if (bean.getHandlerName() != null && !bean.getHandlerName().equals("")) {
					// ec.,
					// com.patterns.jobs.message.value.parser.handler$getPatientRoomSL
					String[] values = bean.getHandlerName().split("\\$");
					String handlerName = values[0];
					String methodName = values[1];

					Class classObject = null;
					Object object = null;
					Method methodObject = null;
					Class[] parameterTypes = new Class[] { Map.class };
					Object[] params = new Object[] { input };
					classObject = Class.forName(handlerName);
					object = classObject.getConstructor(parameterTypes).newInstance(params);
					methodObject = classObject.getMethod(methodName);
					Object result = methodObject.invoke(object);
					if (result instanceof String) {
						value = (String) result;
					}
				}

				if (value != null && !value.trim().equals("")) {
					value = value.trim();
				} else {
					if (bean.getValueDataType().equals("2") || bean.getValueDataType().equals("4")) {
						value = null;
					} else {
						value = "";
					}
				}

				if (bean.getLovrecId() != null && !bean.getLovrecId().equals("")) {
					value = getLovercValue(dbContext, bean.getLovrecId(), value);
				}

				if (configurationBean.isValidationReqd()) {
					validateValue(bean.isMandatory(), value, bean.getValueDataType(), bean.getValueRegex(),
							String.valueOf(index));
				}
				if (bean.getValueDataType().equals("4")) {
					if (value.trim().equals("00000000")) {
						if(bean.isMandatory()){
							recordDetails.put(bean.getColumnName(),
									BackOfficeFormatUtils.getDate("01011900", bean.getDateFormat()));
						}else{
							recordDetails.put(bean.getColumnName(), null);
						}
					} else {
						recordDetails.put(bean.getColumnName(),
								BackOfficeFormatUtils.getDate(value, bean.getDateFormat()));
					}
				} else {
					recordDetails.put(bean.getColumnName(), value == null ? null : value.trim());
				}
			}
			operationType = recordOperation;
		} catch (JobsException e) {
			throw e;
		} catch(InvocationTargetException e1){
			logger.logError("parseRecordDetails() Error1 -Handler Error- ");
			throw new JobsException(JobsErrorCodes.FAILURE_PREPROCESSING_E);
		}catch (Exception ex) {
			logger.logError("parseRecordDetails() Error2 - " + ex.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.ERROR_AUTO_VALIDATION_FAILURE_E);
		} finally {

		}
		logger.logDebug("parseRecordDetails() - End");
		return recordDetails;
	}

	private void validateValue(boolean isMandatory, String value, String valueDataType, String regex, String index)
			throws JobsException {
		logger.logDebug("validateValue() - Begin");
		try {
			if (valueDataType.equals(VALUEDATATYPE_STRING) || valueDataType.equals(VALUEDATATYPE_NUMBER)
					|| valueDataType.equals(VALUEDATATYPE_DECIMAL)) {
				if (isMandatory) {
					if (value == null || value.equals("")) {
						System.out.println("Key code:" + index);
						throw new JobsException(JobsErrorCodes.FAILURE_PARAMETERS_INSUFFICIENT_E);
					}
				}
				if (value != null) {
					if (!isPatternMatched(value, regex)) {
						System.out.println("Validation failed for:" + index);
						throw new JobsException(JobsErrorCodes.FAILURE_PARAMETERS_VALIDATION_E);
					}
				}
			} else if (valueDataType.equals(VALUEDATATYPE_DATE)) {
				if (isMandatory) {
					if (value == null || value.equals("")) {
						System.out.println("Mandatory key " + index);
						throw new JobsException(JobsErrorCodes.FAILURE_PARAMETERS_INSUFFICIENT_E);
					}
				}
				if (value != null && !value.equals("")) {
					if (!isValidDate(value)) {
						throw new JobsException(JobsErrorCodes.FAILURE_PARAMETERS_VALIDATION_E);
					}
				}
			}
		} catch (Exception e) {
			logger.logError(e.getLocalizedMessage());
			throw new JobsException(JobsErrorCodes.ERROR_AUTO_VALIDATION_FAILURE_E);
		}
		logger.logDebug("validateValue() - End");
	}

	private String getLovercValue(DBContext dbContext, String id, String key) {
		logger.logDebug("getLovercValue() - Begin");
		String result = null;
		ResultSet rs = null;
		DBUtil dbUtil = dbContext.createUtilInstance();
		try {
			dbUtil.reset();
			dbUtil.setMode(DBUtil.PREPARED);
			dbUtil.setSql("SELECT VALUE FROM CTJCMLOVREC WHERE ID=? AND NAME=?");
			dbUtil.setString(1, id);
			dbUtil.setString(2, key);
			rs = dbUtil.executeQuery();
			if (rs.next()) {
				result = rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			logger.logError(e.getLocalizedMessage());
		} finally {
			dbUtil.reset();
		}
		logger.logDebug("getLovercValue() - End");
		return result;
	}

	public boolean isValidDate(String value) {
		logger.logDebug("isValidDate() - Begin");
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
		try {
			sdf.parse(value);
		} catch (Exception e) {
			logger.logError(e.getLocalizedMessage());
			return false;
		}
		logger.logDebug("isValidDate() - End");
		return true;
	}

	public boolean isPatternMatched(String value, String regex) {
		logger.logDebug("isPatternMatched() - Begin");
		if (regex == null)
			return false;
		regex = regex.trim();
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(value);
		if (matcher.find()) {
			return true;
		}
		logger.logDebug("isPatternMatched() - End");
		return false;
	}
}
