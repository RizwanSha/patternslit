package com.patterns.jobs.message.handler;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.patterns.jobs.exceptions.JobsException;
import com.patterns.jobs.message.handler.bean.RecordParser;

public class WardTypeMessageHandler extends AbstractMessageHandler {

	@Override
	public void process() throws JobsException {
		if (getRecordList().size() > 0) {
			Map<String, List<Map<String, Object>>> tableDataMap = new HashMap<String, List<Map<String, Object>>>();
			String operationType = "";
			for (String recordContent : getRecordList()) {
				RecordParser parser = new RecordParser(getDbContext(), recordContent, getEntityCode(), getProcessingCode());
				Map<String, Object> recordValue = parser.parseRecordDetails();
				recordValue.put("PROGRAM_ID", getProgramId());
				recordValue.put("ENTITY_CODE", getEntityCode());

				if (parser.getTableType().equals("S")) {
					RecordConfiguration configuration = new RecordConfiguration(parser.getTableName(), getDbContext());
					configuration.getInternalStore().putAll(recordValue);
					if (parser.getOperationType().equals("W")) {
						configuration.setNew(true);
					} else if (parser.getOperationType().equals("R")) {
						configuration.setNew(false);
					}
					configuration.save();
				} else {
					if (!tableDataMap.containsKey(parser.getTableName())) {
						List<Map<String, Object>> recordList = new LinkedList<Map<String, Object>>();
						recordList.add(recordValue);
						tableDataMap.put(parser.getTableName(), recordList);
						operationType = parser.getOperationType();
					} else {
						List<Map<String, Object>> recordList = tableDataMap.get(parser.getTableName());
						recordList.add(recordValue);
						tableDataMap.put(parser.getTableName(), recordList);
					}
				}
			}

			for (String tableName : tableDataMap.keySet()) {
				List<Map<String, Object>> recordList = tableDataMap.get(tableName);

				RecordConfiguration configuration = new RecordConfiguration(tableName, getDbContext());
				configuration.setNew(true);
				// Delete
				configuration.save(recordList);
			}
		}
	}
}
