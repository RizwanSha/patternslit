/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patterns.jobs.exceptions;

/**
 * 
 * @author PATTERNS0004
 */
public class JobsErrorCodes {
	
	public static final String UNKNOWN_E = "INTF9999";

	public static final String PARSING_E = "INTF0001";
	public static final String INVALID_PROCESSING_CODE_E = "INTF0002";
	public static final String REQDATE_UNAVAIL_E = "INTF0003";
	public static final String OUTBOUND_DATA_UNAVAIL_E = "INTF0004";
	public static final String MSG_HANDLER_UNAVAIL_E = "INTF0005";
	public static final String MSG_HANDLER_PROCCESSING_FAIL_E = "INTF0006";
	public static final String DESTQ_CFG_UNAVAIL_E = "INTF0007";
	public static final String CFG_UNAVAIL_E = "INTF0008";
	
	public static final String FAILURE_PARAMETERS_INSUFFICIENT_E = "INTF0009";
	public static final String FAILURE_PARAMETERS_VALIDATION_E = "INTF0010";
	public static final String ERROR_AUTO_VALIDATION_FAILURE_E = "INTF0011";
	public static final String FAILURE_PREPROCESSING_E = "INTF0012";
	public static final String DATABASE_UPDATE_E = "INTF0013";
	public static final String OPERATION_E = "INTF0014";
	
	
	public static final String PARSING_E_DESC = "Parsing Error";
	public static final String INVALID_PROCESSING_CODE_EDESC = "Invalid Processing Code";
	public static final String REQDATE_UNAVAIL_EDESC = "Request Data Unavailable";
	public static final String OUTBOUND_DATA_UNAVAIL_EDESC = "Outbound Data Unavailable";
	public static final String MSG_HANDLER_UNAVAIL_EDESC = "Message Handler Unavailable";
	public static final String MSG_HANDLER_PROCCESSING_FAIL_EDESC = "Message Handler Processing Failure";
	public static final String DESTQ_CFG_UNAVAIL_EDESC = "Destination Queue Configuration Unavailable";
	public static final String CFG_UNAVAIL_EDESC = "Configuration not defined for the message";

	public static final String EMAIL_PROCESS_THREADEXEC_INTR_E = "MAILPUE0001";
	public static final String EMAIL_PROCESS_THREADEXEC_E = "MAILPUE0002";
	public static final String EMAIL_PROCESS_THREADEXEC_EXEC_E = "MAILPUE0003";
	public static final String EMAIL_PROCESS_NOPROVIDER_E = "MAILPUE0004";
	public static final String EMAIL_PROCESS_MSG_EXEC_E = "MAILPUE0005";
	public static final String EMAIL_PROCESS_ADDR_EXEC_E = "MAILPUE0006";
	public static final String EMAIL_PROCESS_ENCODE_EXEC_E = "MAILPUE0007";
	
	
	

}
