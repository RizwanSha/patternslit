/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patterns.jobs.exceptions;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 *
 * @author PATTERNS0004
 */
public class JobsException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = -6830973080539421950L;

	String errorCode;

    private static ResourceBundle _ERROR_BUNDLE;

    static {
        try {
            _ERROR_BUNDLE = ResourceBundle.getBundle("com.patterns.jobs.exceptions.ErrorMessages");
        } catch (MissingResourceException e) {
            e.printStackTrace();
        }
    }

    public JobsException(String errorCode) {
        super(_ERROR_BUNDLE.getString(errorCode));
        this.errorCode = errorCode;
    }

    public JobsException(String errorCode, Throwable cause) {
        super(_ERROR_BUNDLE.getString(errorCode), cause);
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

}
