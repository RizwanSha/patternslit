package patterns.config.framework.appserver;

public class ApplicationServerUtils {

	public static final int JBOSS_APP_SERVER = 1;
	public static final int OTHER_APP_SERVER = 2;

	private static int applicationServerClass = 0;

	static {
		applicationServerClass = OTHER_APP_SERVER;
		try {
			Class.forName("org.jboss.resource.adapter.jdbc.WrappedConnection");
			applicationServerClass = JBOSS_APP_SERVER;
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("Assuming Non-JBOSS Server");
		}
	}

	public static int getApplicationServerClass() {
		return applicationServerClass;
	}
}