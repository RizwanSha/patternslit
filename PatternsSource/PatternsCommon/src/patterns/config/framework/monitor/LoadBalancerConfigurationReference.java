package patterns.config.framework.monitor;

import patterns.config.framework.web.filters.LoadBalancerConfiguration;

public class LoadBalancerConfigurationReference {

	private static LoadBalancerConfiguration loadBalancerConfiguration = null;

	public static void setConfiguration(LoadBalancerConfiguration configuration) {
		loadBalancerConfiguration = configuration;
	}

	public static LoadBalancerConfiguration getLoadBalancerConfiguration() {
		return loadBalancerConfiguration;
	}
}