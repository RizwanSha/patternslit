package patterns.config.framework.monitor;

import patterns.config.framework.loggers.ApplicationLogger;
import patterns.config.framework.service.DTObject;

public class JobApplicationContext {

	private final ApplicationLogger logger;

	private JobApplicationContext() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
		logger.logDebug("()");
	}

	public static JobApplicationContext getInstance() {
		return ContextReferenceHelper._INSTANCE;
	}

	private static class ContextReferenceHelper {
		private static final JobApplicationContext _INSTANCE = new JobApplicationContext();
	}

	private volatile DTObject initParameters;

	public void setContext(DTObject initParameters) {
		this.initParameters = initParameters;
	}

	public DTObject getContext() {
		return initParameters;
	}
}