package patterns.config.framework.web.ajax;

import patterns.config.framework.database.utils.DBContext;

public abstract class ContentManager {

	public static final String ACTION = "ACTION";
	public static final String RECTIFY = "RECTIFY";
	public static final String RECORD_FINDER_REQUEST = "2";
	public static final String LOOKUP_RESOLVE_REQUEST = "3";
	public static final String RECORD_RESOLVE_REQUEST = "4";
	public static final String MTM_RESOLVE_REQUEST = "5";
	public static final String GRIDQUERY_RESOLVE_REQUEST = "6";
	public static final String LOOKUP_MANAGER_REQUEST = "1";
	public static final String REFLECTION_CLASS = "_ReflectionClass";
	public static final String REFLECTION_METHOD = "_ReflectionMethod";
	public static final String REPORT_KEY = "REPORT";
	public static final String REPORT_FORMAT = "REPORT_FORMAT";
	public static final String REPORT_HANDLER = "REPORT_HANDLER";
	public static final String REPORT_ID = "reportID";
	public static final String INVENTORY_NUMBER = "inventoryNumber";
	public static final String TOKEN_KEY = "_TOKEN";
	public static final String PROGRAM_KEY = "_PROGRAM";
	public static final String METHOD_KEY = "_METHOD";
	public static final String ARGUMENT_KEY = "_ARGS";
	public static final String BUFFER_KEY = "count";
	public static final String INIT_KEY = "init";
	public static final String START_POSITION_KEY = "posStart";
	public static final String DEFAULT_BUFFER = "60";
	public static final String DEFAULT_POSITION = "0";
	public static final String SEARCH_COLUMN = "_SRCHCOL";
	public static final String SEARCH_TEXT = "_SRCHTXT";
	public static final String SORT_ORDER = "_SORTORDER";
	public static final String SORT_ASC = "ASC";
	public static final String SORT_DESC = "DESC";
	public static final String ORDER_COLUMN = "_ORDERCOL";
	public static final String MAIN_TABLE = "MAIN_TABLE";
	public static final String DETAIL_TABLE = "DETAIL_TABLE";
	public static final String ROW_NOT_PRESENT = "Row Not Present";
	public static final String ROW_PRESENT = "Row Present";
	
	public static final String DEDUP_KEY = "DEDUP_KEY";
	public static final String PURPOSE_ID = "PURPOSE_ID";
	public static final String PROGRAM_ID = "PROGRAM_ID";

	public static final String RECORD = "record";
	public static final String ROWS = "rows";
	public static final String ERROR = "error";
	public static final String ERROR_PARAM = "error_param";
	public static final String ERROR_FIELD = "error_field";
	public static final String ERRORS = "errors";
	public static final String ELEMENT = "element";
	public static final String MESSAGE = "message";
	public static final String PARAMS = "params";
	public static final String PARAM = "param";
	public static final String KEY = "key";
	public static final String VALUE = "value";
	public static final String RESPONSE = "response";
	public static final String TRANSACTION = "transaction";
	public static final String STATUS = "status";
	public static final String ERROR_MESSAGE = "error_message";
	public static final String ERROR_ARGS = "error_args";
	public static final String RESULT = "result";
	public static final String RESULT_XML = "result_xml";
	public static final String CONTENT = "content";
	public static final String DATA_AVAILABLE = "1";
	public static final String DATA_UNAVAILABLE = "0";
	public static final String EMPTY_STRING = "";
	public static final String TBA_RECORD = "tbarecord";
	public static final String PROFESSIONAL_TITLE="P";
	public static final String GENERAL_TITLE="G";
	public static final String AUTHORIZED_ENTRY="A";
	public static final String REJECTED_ENTRY="R";
	public static final String TABLE = "table";
	public static final String FETCH = "fetch";
	public static final String FETCH_COLUMNS = "fetchColumns";
	public static final String FETCH_ALL = "1";
	public static final String FETCH_DESC = "0";

	public static final String E_STATUS = "E_STATUS";
	public static final String AUTH_ON = "AUTH_ON";
	public static final String REJ_ON = "REJ_ON";
	
	public static final String CODE = "CODE";
	public static final String COLUMN = "COLUMN";

	public static final String ENTITY_CODE = "ENTITY_CODE";
	public static final String USER_ACTION = "ACTION";

	public static final String USER_ID = "USER_ID";
	public static final String CUSTOMER_CODE = "CUSTOMER_CODE";
	public static final String SRV_REF_NUM = "SRV_REF_NUM";
	public static final String CURRENCY = "CURRENCY";
	public static final String ITR_SL = "ITR_SL";
	public static final String SERVICE_FILE_BASED = "1";
	public static final String SERVICE_SL_REQD = "2";
	public static final String SERVICE_STMT_PGM = "3";
	public static final String SERVICE_REQ_LIMITS = "4";
	public static final String SERVICE_FIN_LIMITS = "5";
	public static final String SERVICE_TFA_REQ = "6";
	public static final String SERVICE_SLA_REQ = "7";
	public static final String SERVICE_SCH_REQ = "8";
	public static final String SERVICE_BRN_REQ = "9";
	public static final String SERVICE_ISSUETYPE_REQ = "10";
	public static final String SERVICE_CURR_LIMITS = "11";
	public static final String SERVICE_GRIDRESTRICT_REQ = "12";
	public static final String SERVICE_BALCHECK_REQ = "13";

	public static final String SESSION_USER_ID = "#USER_ID#";
	public static final String SESSION_PARTITION_NO = "#PARTITION_NO#";
	public static final String SESSION_ENTITY_CODE = "#ENTITY_CODE#";
	public static final String SESSION_CUSTOMER_CODE = "#CUSTOMER_CODE#";
	public static final String SESSION_DATE_FORMAT = "#DATE_FORMAT#";
	public static final String SESSION_DATETIME_FORMAT = "#DATETIME_FORMAT#";
	public static final String SESSION_CBD = "#CBD#";
	public static final String SESSION_TBA_FORMAT = "#TBA_FORMAT#";
	public static final String SESSION_BRANCH_CODE = "#BRANCH_CODE#";
	public static final String SESSION_ROLE_CODE = "#ROLE_CODE#";
	public static final String SESSION_ROLE_TYPE = "#ROLE_TYPE#";
	public static final String SESSION_ACCOUNT_DOMAIN = "#ACCOUNT_DOMAIN#";
	public static final String SESSION_SERVICE_MODEL = "#SERVICE_MODEL#";
	public static final String SESSION_SERVICE_BOUQUET = "#SERVICE_BOUQUET#";
	public static final String SESSION_USER_GROUP = "#USER_GROUP#";
	public static final String YEAR_PARAM = "YEAR_PARAM";
	public static final String PARTITION_NO = "PARTITION_NO";
	public static final String ALPHA_CODE = "ALPHA_CODE";
	public static final String SESSION_CLUSTER_CODE = "#CLUSTER_CODE#";
	public static final String SESSION_FIN_YEAR = "#FIN_YEAR#";
	private DBContext dbContext = null;

	public final void init() {
		dbContext = new DBContext();
	}

	public final void destroy() {
		if (dbContext != null)
			dbContext.close();
	}

	public DBContext getDbContext() {
		return dbContext;
	}
}