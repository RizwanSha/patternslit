package patterns.config.framework.web.ajax;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ErrorMap {

	private Map<String, String> internalMap;
	private Map<String, Object[]> internalMapParam;

	public ErrorMap() {
		internalMap = new HashMap<String, String>();
		internalMapParam = new HashMap<String, Object[]>();
	}

	public void setError(String field, String key) {
		internalMap.put(field, key);
	}

	public void setError(String field, String key, Object[] params) {
		internalMap.put(field, key);
		internalMapParam.put(field, params);
	}

	public Set<String> getFields() {
		return internalMap.keySet();
	}

	public String getErrorKey(String field) {
		return internalMap.get(field);
	}

	public Object[] getErrorParam(String field) {
		return internalMapParam.get(field);
	}

	public int length() {
		return internalMap.size();
	}

	public void clear() {
		internalMap.clear();
	}

	public Map<String, String> getMap() {
		return internalMap;
	}

}
