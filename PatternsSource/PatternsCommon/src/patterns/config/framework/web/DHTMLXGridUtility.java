package patterns.config.framework.web;

import java.util.Map;

import patterns.config.framework.database.RegularConstants;
import patterns.config.framework.monitor.ContextReference;

public class DHTMLXGridUtility {

	public static class Commands {
		public static final String GROUP_BY = "groupBy";
		public static final String SET_SKIN = "setSkin";
		public static final String ATTACH_FOOTER = "attachFooter";
		public static final String ATTACH_HEADER = "attachHeader";
	}

	public static class Columns {
		public static final String SELECT_WIDTH = "0";
		public static final String SELECT_TYPE = "ch";
		public static final String SET_SKIN = "setSkin";
		public static final String ATTACH_FOOTER = "attachFooter";
		public static final String ATTACH_HEADER = "attachHeader";
	}

	public static class ColumnTypes {
		public static final String CHECK_BOX = "ch";
		public static final String READ_ONLY = "ro";
		public static final String EDITABLE = "ed";
	}

	public static class ColumnWidths {
		public static final String ZERO_WIDTH = "0";
		public static final String CRDB_WIDTH = "40";
		public static final String SELECT_WIDTH = "70";
		public static final String CURRENCY_WIDTH = "90";
		public static final String MEDIUM_DESCRIPTION = "160";
		public static final String LARGE_DESCRIPTION = "200";
		public static final String SMALL_DESCRIPTION = "140";
		public static final String AMOUNT_WIDTH = "130";
	}

	public static class ColumnAlignment {
		public static final String LEFT = "left";
		public static final String RIGHT = "right";
		public static final String CENTER = "center";
	}

	private int i = 0;
	private int cellID = 0;
	StringBuffer buffer = null;

	public DHTMLXGridUtility() {
		buffer = new StringBuffer(2048);
	}

	public void init(long count, long startPosition) {
		buffer.append("<?xml version='1.0' encoding='UTF-8'?>");
		buffer.append("<rows total_count='" + count + "' pos='" + startPosition + "' session-expired='0'>");
	}

	public void init() {
		buffer.append("<?xml version='1.0' encoding='UTF-8'?>");
		buffer.append("<rows session-expired='0'>");
	}

	public void init(boolean sessionExpired) {
		buffer.append("<?xml version='1.0' encoding='UTF-8'?>");
		if (sessionExpired) {
			buffer.append("<rows session-expired='1'>");
		} else {
			buffer.append("<rows session-expired='0'>");
		}
	}

	public void finish() {
		buffer.append("</rows>");
	}

	public void startHead() {
		buffer.append("<head>");
	}

	public void endHead() {
		buffer.append("</head>");
	}

	public void setColumn(String text, String width, String type, String align) {
		buffer.append("<column ");
		buffer.append(" width='").append(width).append("'");
		buffer.append(" type='").append(type).append("'");
		buffer.append(" align='").append(align).append("'");
		buffer.append(" >");
		buffer.append(encodeData(text));
		buffer.append("</column>");
	}

	public void setColumn(String text, String width, String type, String align, String sort) {
		buffer.append("<column ");
		buffer.append(" width='").append(width).append("'");
		buffer.append(" type='").append(type).append("'");
		buffer.append(" align='").append(align).append("'");
		buffer.append(" sort='").append(sort).append("'");
		buffer.append(" >");
		buffer.append(encodeData(text));
		buffer.append("</column>");
	}

	public void setBeforeInitCommand(String command, String... params) {
		buffer.append("<beforeInit>");
		buffer.append("<call command='").append(command).append("'>");
		for (String param : params) {
			buffer.append("<param>").append(param).append("</param>");
		}
		buffer.append("</call>");
		buffer.append("</beforeInit>");

	}

	public void setafterInitCommand(String command, String... params) {
		buffer.append("<afterInit>");
		buffer.append("<call command='").append(command).append("'>");
		for (String param : params) {
			buffer.append("<param>").append(param).append("</param>");
		}
		buffer.append("</call>");
		buffer.append("</afterInit>");
	}

	public void setSkin(String skinName) {
		String[] params = { skinName };
		setafterInitCommand(Commands.SET_SKIN, params);
	}

	public void setGridData(String data) {
		buffer.append(data);
	}

	public void startRow(String id, String style) {
		cellID = 0;
		buffer.append("<row id='").append(id).append("' ");
		if (style != null) {
			buffer.append(" style='").append(style).append("' ");
		}
		buffer.append(" >");
	}

	public void startRow(String id) {
		cellID = 0;
		buffer.append("<row id='").append(id).append("' >");
	}

	public void startRow(String id, Map<String, String> attributes) {
		cellID = 0;
		buffer.append("<row id='").append(id).append("' ");
		for (Map.Entry<String, String> mapEntry : attributes.entrySet()) {
			buffer.append(" ").append(mapEntry.getKey()).append("='").append(mapEntry.getValue()).append("' ");
		}
		buffer.append(" >");
	}

	public void startRow() {
		cellID = 0;
		buffer.append("<row id='").append(++i).append("' >");
	}

	public void endRow() {
		buffer.append("</row>");
	}

	public void setCell(String data) {
		++cellID;
		buffer.append("<cell id='").append(cellID).append("'>").append(encodeData(data)).append("</cell>");
	}

	public void setCell(String data, String attributeName, String attributeValue) {
		++cellID;
		buffer.append("<cell id='").append(cellID).append("' ");
		buffer.append(" ").append(attributeName).append("='").append(attributeValue).append("' ");
		buffer.append(" >").append(encodeData(data)).append("</cell>");
	}

	public void setCell(String data, String style) {
		++cellID;
		buffer.append("<cell id='").append(cellID).append("' ");
		if (style != null) {
			buffer.append(" style='").append(style).append("' ");
		}
		buffer.append(" >").append(encodeData(data)).append("</cell>");
	}

	public String getXML() {
		buffer.trimToSize();
		return buffer.toString();
	}

	private String encodeData(String data) {
		if (data == null)
			return RegularConstants.EMPTY_STRING;
		return data.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;");
	}

	public void setData(String data) {
		buffer.append(data);
	}

	public void setError(String error) {
		buffer.append("<error>").append(encodeData(error)).append("</error>");
	}

	public static final String getXMLDefinition(String path) {
		return StringUtils.getString(ContextReference.getContext().getResourceAsStream(path));
	}

	public void appendContent(String content) {
		buffer.append(content);
	}

	public StringBuffer createHeader(long count, long startPosition, String prevReqd, String nextReqd) {
		StringBuffer buffer = new StringBuffer("<?xml version='1.0' encoding='UTF-8'?>");
		buffer.append("<rows total_count='" + count + "' pos='" + startPosition + "' next_reqd='" + nextReqd + "' prev_reqd='" + prevReqd + "' session-expired='0'>");
		return buffer;
	}
}
