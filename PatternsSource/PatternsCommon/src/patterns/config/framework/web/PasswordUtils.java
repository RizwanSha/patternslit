package patterns.config.framework.web;

import java.security.MessageDigest;
import java.security.SecureRandom;
import java.sql.Date;
import java.sql.ResultSet;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;

public class PasswordUtils {

	public static final String ALPHA = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	public static final String NUMERIC = "0123456789";
	public static final String SPECIAL_CHARACTERS = "<=:`[?;,\\^_}@>.)\"('$#*%~-/&!]{|+";
	public static final String GENERATION_SPECIAL_CHARACTERS = "@#$%^*()+!-=_";
	public static final String GENERATION_COMBINATION = ALPHA + NUMERIC + GENERATION_SPECIAL_CHARACTERS;
	private static final int MIN_PASSWORD_LENGTH = 8;
	private static final int MIN_ALPHA_LENGTH = 4;
	private static final int MIN_NUMERIC_LENGTH = 4;
	private static final int MIN_SPECIAL_LENGTH = 0;

	private static byte[] doSHA256(byte[] input) {
		byte[] output = null;
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			output = digest.digest(input);
		} catch (Exception e) {
			e.printStackTrace();
			output = input;
		}
		return output;
	}

	private static String toHexString(byte[] data) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < data.length; ++i) {
			String s = Integer.toHexString(data[i] & 0XFF);
			buf.append((s.length() == 1) ? ("0" + s) : s);
		}
		return buf.toString();
	}

	public static final String encryptData(String key) {
		return key;
	}

	public static final String getEncrypted(String password, String userID, String salt) {
		String shaValue1 = getEncryptedUserIDPassword(password, userID);
		String shaValue2 = getEncryptedHashSalt(shaValue1, salt);
		return shaValue2;
	}

	public static final String getEncryptedUserIDPassword(String password, String userID) {
		String shaValue1 = toHexString(doSHA256(password.getBytes()));
		String shaValue2 = toHexString(doSHA256((shaValue1 + userID).getBytes()));
		return shaValue2;
	}

	public static final String getEncryptedHashSalt(String hash, String salt) {
		String shaFinal = toHexString(doSHA256((hash + salt).getBytes()));
		return shaFinal;
	}

	public static final String getSalt() {
		SecureRandom random = new SecureRandom((System.currentTimeMillis() + "" + System.nanoTime()).getBytes());
		int index = 0;
		StringBuffer buffer = new StringBuffer(128);
		for (int i = 0; i < 128; ++i) {
			index = random.nextInt(GENERATION_COMBINATION.length());
			buffer.append(GENERATION_COMBINATION.charAt(index));
		}
		String shaFinal = toHexString(doSHA256(buffer.toString().getBytes()));
		return shaFinal;
	}

	public static final String generateRandomNonce(String userID, String ipAddress) {
		StringBuffer randomString = new StringBuffer(128);
		randomString.append(userID);
		randomString.append(ipAddress);
		randomString.append(System.currentTimeMillis());
		randomString.append(System.nanoTime());
		randomString.trimToSize();
		SecureRandom random = new SecureRandom(randomString.toString().getBytes());
		int index = 0;
		StringBuffer buffer = new StringBuffer(128);
		for (int i = 0; i < 128; ++i) {
			index = random.nextInt(GENERATION_COMBINATION.length());
			buffer.append(GENERATION_COMBINATION.charAt(index));
		}
		String shaFinal = toHexString(doSHA256(buffer.toString().getBytes()));
		return shaFinal;
	}

	public static void displayPassword() {
		String userID = "ADMINMAKER";
		String password = "laser@123";
		String salt = getSalt();
		String encryptedPassword = getEncrypted(password, userID, salt);
		System.out.println("User ID : " + userID);
		System.out.println("Encrypted Password: " + encryptedPassword);
		System.out.println("Salt : " + salt);
	}

	public static void main(String[] args) throws Exception {
		// generateCUG();
		// generateCUGAllocation();
		// generatePasswords();
		displayPassword();

	}

	public static final String generatePassword(String userID, int minLength, int minAlpha, int minNumeric, int minSpecial) {
		byte[] seedData = (userID + System.currentTimeMillis() + "" + System.nanoTime()).getBytes();
		SecureRandom random = new SecureRandom(seedData);
		String password = null;
		StringBuffer buffer = new StringBuffer();
		int lengthCounter = 0;
		int partCounter = 0;
		int index = 0;
		if (minLength == 0)
			minLength = MIN_PASSWORD_LENGTH;
		if (minAlpha == 0)
			minAlpha = MIN_ALPHA_LENGTH;
		if (minNumeric == 0)
			minNumeric = MIN_NUMERIC_LENGTH;
		if (minSpecial == 0)
			minSpecial = MIN_SPECIAL_LENGTH;
		while (partCounter < minAlpha) {
			index = random.nextInt(ALPHA.length());
			buffer.append(ALPHA.charAt(index));
			partCounter++;
			lengthCounter++;
		}
		partCounter = 0;
		while (partCounter < minNumeric) {
			index = random.nextInt(NUMERIC.length());
			buffer.append(NUMERIC.charAt(index));
			partCounter++;
			lengthCounter++;
		}
		partCounter = 0;
		while (partCounter < minSpecial) {
			index = random.nextInt(GENERATION_SPECIAL_CHARACTERS.length());
			buffer.append(GENERATION_SPECIAL_CHARACTERS.charAt(index));
			partCounter++;
			lengthCounter++;
		}
		while (lengthCounter < minLength) {
			index = random.nextInt(ALPHA.length());
			buffer.append(ALPHA.charAt(index));
			lengthCounter++;
		}
		buffer.trimToSize();
		password = buffer.toString();
		return password;
	}

	public static String generateUserPassword(DBContext dbContext, String entityCode, String userID, Date cbd) {
		String password = null;
		DBUtil dbutil = dbContext.createUtilInstance();
		try {
			dbutil.reset();
			dbutil.setMode(DBUtil.PREPARED);
			dbutil.setSql("SELECT OC.MIN_PWD_LEN,OC.MIN_PWD_NUM,OC.MIN_PWD_ALPHA,OC.MIN_PWD_SPECIAL FROM SYSCPM OC WHERE OC.ENTITY_CODE = ? AND OC.EFFT_DATE = (SELECT MAX(IC.EFFT_DATE) FROM SYSCPM IC WHERE IC.ENTITY_CODE = OC.ENTITY_CODE AND IC.EFFT_DATE <= ?)");
			dbutil.setString(1, entityCode);
			dbutil.setDate(2, cbd);
			ResultSet rset = dbutil.executeQuery();
			if (rset.next()) {
				int minLength = rset.getInt(1);
				int minNumeric = rset.getInt(2);
				int minAlpha = rset.getInt(3);
				int minSpecial = rset.getInt(4);
				password = generatePassword(userID, minLength, minAlpha, minNumeric, minSpecial);
			}
		} catch (Exception e) {
			password = null;
		} finally {
			dbutil.reset();
		}
		return password;
	}

}
