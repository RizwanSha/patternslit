package patterns.config.framework.web;

import java.security.SecureRandom;

public class SecurityUtils {

	private static final int CSRF_TOKEN_LENGTH = 32;

	private static SecureRandom getSecureRandom() {
		StringBuffer s = new StringBuffer();
		s.append(System.currentTimeMillis()).append(System.nanoTime());
		s.trimToSize();
		SecureRandom random = new SecureRandom(s.toString().getBytes());
		return random;
	}

	public static final String generateCSRFToken() {
		return RandomGenerator.generateRandomId(getSecureRandom(), CSRF_TOKEN_LENGTH);
	}

}
