package patterns.config.framework.web;

import java.io.Serializable;
import java.util.ArrayList;

public class GenericOptionGroup implements Serializable {

	private static final long serialVersionUID = 5219179101946440898L;
	private ArrayList<GenericOption> optionList;
	private String label;
	private int counter = 0;

	public ArrayList<GenericOption> getOptionList() {
		return optionList;
	}

	public String getLabel() {
		return this.label;
	}

	public GenericOptionGroup(String label) {
		this.label = label;
		this.optionList = new ArrayList<GenericOption>();
	}

	public void addGenericOption(String id, String label) {
		this.optionList.add(counter, new GenericOption(id, label));
		counter++;
	}
}