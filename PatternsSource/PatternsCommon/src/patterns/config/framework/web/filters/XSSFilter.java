package patterns.config.framework.web.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import patterns.config.framework.loggers.ApplicationLogger;

public class XSSFilter implements Filter {

	ApplicationLogger logger = null;

	public XSSFilter() {
		super();
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
	}

	public final void init(FilterConfig arg0) throws ServletException {
	}

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		chain.doFilter(new XSSCleanRequestWrapper((HttpServletRequest) request), response);
	}
}