package patterns.config.framework.web.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public abstract class CommonFilter implements Filter {

	public CommonFilter() {
	}

	public final void init(FilterConfig arg0) throws ServletException {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		processRequest(chain);
	}

	public final void destroy() {
	}

	public abstract void processRequest(FilterChain chain) throws IOException, ServletException;
}