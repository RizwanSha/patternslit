package patterns.config.framework.web.filters;

import java.io.Serializable;

public class LoadBalancerConfiguration implements Serializable {

	private static final long serialVersionUID = 3271851929768472896L;

	private boolean schemeEnabled;

	private String scheme;

	private boolean serverNameEnabled;

	private String serverName;

	private boolean serverPortEnabled;

	private int serverPort;

	private boolean contextPathEnabled;

	private String contextPath;

	public boolean isSchemeEnabled() {
		return schemeEnabled;
	}

	public void setSchemeEnabled(boolean schemeEnabled) {
		this.schemeEnabled = schemeEnabled;
	}

	public String getScheme() {
		return scheme;
	}

	public void setScheme(String scheme) {
		this.scheme = scheme;
	}

	public boolean isServerNameEnabled() {
		return serverNameEnabled;
	}

	public void setServerNameEnabled(boolean serverNameEnabled) {
		this.serverNameEnabled = serverNameEnabled;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public boolean isServerPortEnabled() {
		return serverPortEnabled;
	}

	public void setServerPortEnabled(boolean serverPortEnabled) {
		this.serverPortEnabled = serverPortEnabled;
	}

	public int getServerPort() {
		return serverPort;
	}

	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}

	public boolean isContextPathEnabled() {
		return contextPathEnabled;
	}

	public void setContextPathEnabled(boolean contextPathEnabled) {
		this.contextPathEnabled = contextPathEnabled;
	}

	public String getContextPath() {
		return contextPath;
	}

	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}

}