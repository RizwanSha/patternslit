package patterns.config.framework.web.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import patterns.config.framework.loggers.ApplicationLogger;

public class RefererCheckFilter implements Filter {
	ApplicationLogger logger = null;

	public RefererCheckFilter() {
		logger = ApplicationLogger.getInstance(this.getClass().getSimpleName());
	}

	public void init(FilterConfig fConfig) throws ServletException {
	}

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		res.setDateHeader("Expires", System.currentTimeMillis() + (30 * 60 * 1000L));
		if (req.getHeader("referer") == null) {
			logger.logError("#doFilter() - referer null - " + req.getRemoteHost() + req.getRemotePort() + req.getRequestedSessionId());
			res.sendError(HttpServletResponse.SC_FORBIDDEN);
		} else {
			chain.doFilter(request, response);
		}
	}
}