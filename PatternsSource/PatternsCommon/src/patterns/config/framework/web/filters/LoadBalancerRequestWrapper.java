package patterns.config.framework.web.filters;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class LoadBalancerRequestWrapper extends HttpServletRequestWrapper {

	private LoadBalancerConfiguration loadBalancerConfiguration;

	public LoadBalancerRequestWrapper(HttpServletRequest servletRequest, LoadBalancerConfiguration loadBalancerConfiguration) {
		super(servletRequest);
		this.loadBalancerConfiguration = loadBalancerConfiguration;
	}

	public String getScheme() {
		if (loadBalancerConfiguration.isSchemeEnabled()) {
			return loadBalancerConfiguration.getScheme();
		} else {
			return super.getScheme();
		}
	}

	public int getServerPort() {
		if (loadBalancerConfiguration.isServerPortEnabled()) {
			return loadBalancerConfiguration.getServerPort();
		} else {
			return super.getServerPort();
		}
	}

	public String getServerName() {
		if (loadBalancerConfiguration.isServerNameEnabled()) {
			return loadBalancerConfiguration.getServerName();
		} else {
			return super.getServerName();
		}
	}

	public String getContextPath() {
		if (loadBalancerConfiguration.isContextPathEnabled()) {
			return loadBalancerConfiguration.getContextPath();
		} else {
			return super.getContextPath();
		}
	}
}
