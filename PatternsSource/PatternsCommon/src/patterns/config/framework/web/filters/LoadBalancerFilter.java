package patterns.config.framework.web.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import patterns.config.framework.monitor.LoadBalancerConfigurationReference;

public class LoadBalancerFilter implements Filter {

	public LoadBalancerFilter() {
		super();
	}

	public final void init(FilterConfig arg0) throws ServletException {
	}

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		LoadBalancerConfiguration loadBalancerConfiguration = LoadBalancerConfigurationReference.getLoadBalancerConfiguration();
		chain.doFilter(new LoadBalancerRequestWrapper((HttpServletRequest) request, loadBalancerConfiguration), response);
	}
}