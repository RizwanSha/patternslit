package patterns.config.framework.web;

import patterns.config.framework.database.RegularConstants;

public class HTMLGridUtility {

	public static class ColumnAlignment {
		public static final String LEFT = "left";
		public static final String RIGHT = "right";
		public static final String CENTER = "center";
	}

	StringBuffer buffer = null;

	public HTMLGridUtility() {
		buffer = new StringBuffer(2048);
	}

	public void init() {
		buffer.append("<table cellpadding='0' cellspacing='0' class='display-grid'>");
	}

	public void init(String className) {
		buffer.append("<table cellpadding='0' cellspacing='0' class='display-grid ").append(className).append("'>");
	}

	public void finish() {
		buffer.append("</table>");
	}

	public void startHead() {
		buffer.append("<thead>");
		buffer.append("<tr>");
	}

	public void endHead() {
		buffer.append("</thead>");
		buffer.append("</tr>");
	}

	public void setColumn(String text, String width, String align) {
		buffer.append("<th ");
		buffer.append(" width='").append(width).append("'");
		buffer.append(" align='").append(align).append("'");
		buffer.append(" >");
		buffer.append(text);
		buffer.append("</th>");
	}

	public void startRow() {
		buffer.append("<tr>");
	}

	public void startRow(String attributeKey[], String attributeValue[]) {
		buffer.append("<tr ");
		for(int i=0;i<attributeKey.length;i++){
			buffer.append(attributeKey[i]).append("='").append(attributeValue[i]).append("'");
		}
		buffer.append(">");
		

	}
	
	public void endRow() {
		buffer.append("</tr>");
	}

	public void setCell(String data, boolean encode) {
		if (encode) {
			buffer.append("<td>").append(encodeData(data)).append("</td>");
		} else {
			buffer.append("<td>").append(data).append("</td>");
		}

	}

	public void setCell(String data) {
		buffer.append("<td>").append(data).append("</td>");
	}

	public void setCell(String data, String attributeKey, String attributeValue, boolean encode) {
		if (encode) {
			buffer.append("<td ").append(attributeKey).append("='").append(attributeValue).append("' >").append(encodeData(data)).append("</td>");
		} else {
			buffer.append("<td ").append(attributeKey).append("='").append(attributeValue).append("' >").append(data).append("</td>");
		}

	}
	
	public void setCell(String data, String attributeKey[], String attributeValue[], boolean encode) {
		buffer.append("<td ");
		for(int i=0;i<attributeKey.length;i++){
			buffer.append(attributeKey[i]).append("='").append(attributeValue[i]).append("'");
		}
		if (encode) {
			buffer.append(">").append(encodeData(data)).append("</td>");
		} else {
			buffer.append(">").append(data).append("</td>");
		}

	}

	public void setCell(String data, String attributeKey, String attributeValue) {
		buffer.append("<td ").append(attributeKey).append("='").append(attributeValue).append("' >").append(data).append("</td>");
	}

	public String getHTML() {
		buffer.trimToSize();
		return buffer.toString();
	}

	private String encodeData(String data) {
		if (data == null)
			return RegularConstants.EMPTY_STRING;
		return data.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;");
	}
}
