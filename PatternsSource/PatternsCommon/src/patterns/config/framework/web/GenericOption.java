package patterns.config.framework.web;

import java.io.Serializable;

public class GenericOption implements Serializable {

	private static final long serialVersionUID = 5219179101946440898L;
	private String id;
	private String label;

	public String getId() {
		return this.id;
	}

	public String getLabel() {
		return this.label;
	}

	public GenericOption(String id, String label) {
		this.id = id;
		this.label = label;
	}
}
