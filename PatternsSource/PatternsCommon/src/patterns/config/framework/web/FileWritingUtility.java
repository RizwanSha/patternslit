package patterns.config.framework.web;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;

public class FileWritingUtility {
	public static final int BUFFER_SIZE = 8024;

	public boolean writeToFile(String path, byte[] data) {
		boolean written = false;
		BufferedOutputStream bos = null;
		try {
			bos = new BufferedOutputStream(new FileOutputStream(path), BUFFER_SIZE);
			bos.write(data);
			written = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bos != null) {
				try {
					bos.close();
				} catch (Exception e) {
				}
			}
		}
		return written;
	}

	public boolean writeToFile(String path, String data) {
		boolean written = false;
		BufferedWriter bos = null;
		try {
			bos = new BufferedWriter(new FileWriter(path), BUFFER_SIZE);
			bos.write(data);
			written = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bos != null) {
				try {
					bos.close();
				} catch (Exception e) {
				}
			}
		}
		return written;
	}
}