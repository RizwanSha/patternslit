package patterns.config.framework.web;

public class CSVUtility {

	public static class DelimiterSeparator {
		public static final String DL_TAB = "\t";
		public static final String DL_COMMA = ",";
		public static final String DL_TILDE = "~";
	}

	public static class RecordSeparator {
		public static final String RS_CRLF = "\r\n";
		public static final String RS_CR = "\r";
		public static final String RS_LF = "\n";
	}

	StringBuffer buffer = null;

	String delimiterSeparator;
	String recordSeparator;

	public CSVUtility(String delimiterSeparator, String recordSeparator) {
		buffer = new StringBuffer(2048);
		this.delimiterSeparator = delimiterSeparator;
		this.recordSeparator = recordSeparator;
	}

	public void init() {
	}

	public void finish() {
	}

	public void startHead() {
	}

	public void endHead() {
		int lastDelimitorPosition = buffer.lastIndexOf(delimiterSeparator);
		buffer.replace(lastDelimitorPosition, lastDelimitorPosition + 1, recordSeparator);
	}

	public void startRow() {
	}

	public void endRow() {
		int lastDelimitorPosition = buffer.lastIndexOf(delimiterSeparator);
		buffer.replace(lastDelimitorPosition, lastDelimitorPosition + 1, recordSeparator);
	}

	public void setCell(String data) {
		if (data != null) {
			data = data.replace(delimiterSeparator, " ");
		}
		buffer.append(data).append(delimiterSeparator);
	}

	public String getData() {
		buffer.trimToSize();
		return buffer.toString();
	}
}