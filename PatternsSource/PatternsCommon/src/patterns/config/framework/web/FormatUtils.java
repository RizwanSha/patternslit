package patterns.config.framework.web;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import patterns.config.framework.database.RegularConstants;

public class FormatUtils {

	private static String convertSimpleFormat(String format) {
		if (format.equals("dd/MM/yyyy"))
			return "dd/MM/yyyy";
		else if (format.equals("%d/%m/%Y"))
			return "dd/MM/yyyy";
		else if (format.equals("EEEE dd , MMMM YYYY"))
			return "EEEE dd , MMMM YYYY";
		
		else
			return format;
	}

	public static String getDate(Date date, String format) {
		if (date == null)
			return null;
		format = convertSimpleFormat(format);
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String result = null;
		result = sdf.format(date);
		return result;
	}

	public static Date getDate(String date, String format) {
		if (date == null || date.trim().equals(RegularConstants.EMPTY_STRING))
			return null;
		format = convertSimpleFormat(format);
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		Date result = null;
		try {
			result = sdf.parse(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public static String[] splitKey(String key) {
		return key.split(RegularConstants.PK_SEPARATOR);
	}

	public static boolean decodeStringToBoolean(String value) {
		if (value != null && value.equals(RegularConstants.COLUMN_ENABLE))
			return true;
		return false;
	}

	public static String decodeBooleanToString(boolean value) {
		return value ? RegularConstants.COLUMN_ENABLE : RegularConstants.COLUMN_DISABLE;
	}

	public static String getDate(String date, String format1, String format2) {
		Date dateValue = getDate(date, format1);
		String result = getDate(dateValue, format2);
		return result;
	}

	public static String formatNumber(String value, String pattern) {
		DecimalFormat format = (DecimalFormat) DecimalFormat.getInstance(new Locale("en", "IN"));
		format.setRoundingMode(RoundingMode.DOWN);
		format.setMinimumFractionDigits(2);
		format.setMaximumFractionDigits(2);
		format.setMaximumIntegerDigits(27);
		// format.applyPattern(pattern);
		String result = format.format(new BigDecimal(value));
		return result;
	}

	// Aribala added
	public static XMLGregorianCalendar getGregorianDate(String date, String format) {
		XMLGregorianCalendar xmlGrogerianCalendar = null;
		try {
			Date utilDate = FormatUtils.getDate(date, format);
			GregorianCalendar gregorianCalendar = new GregorianCalendar();
			gregorianCalendar.setTime(utilDate);
			xmlGrogerianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
		} catch (Exception e) {
			e.printStackTrace();
			Date utilDate = FormatUtils.getDate(date, "yyyyMMdd");
			GregorianCalendar gregorianCalendar = new GregorianCalendar();
			gregorianCalendar.setTime(utilDate);
			try {
				xmlGrogerianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
			} catch (DatatypeConfigurationException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return xmlGrogerianCalendar;
	}

	public static Date getUtilDate(XMLGregorianCalendar calendar) {
		if (calendar == null) {
			return null;
		}
		return calendar.toGregorianCalendar().getTime();
	}

	// Aribala added

	public static void main(String[] args) {
		// System.out.println(formatNumber("123456789012345678901234567.999", "##,##,##,###,##,##,###,##,##,###,##,##,##0.0#"));
		System.out.println(formatNumber(unformatNumber("1,23,456,78,90,123,45,67,890,12,34,567.969"), "#0,#0,##0.00"));
	}

	public static String unformatNumber(String number) {
		return number.replace(",", "");
	}

}
