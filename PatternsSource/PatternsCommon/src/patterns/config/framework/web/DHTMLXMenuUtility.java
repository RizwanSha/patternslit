package patterns.config.framework.web;

import java.util.Map;

import patterns.config.framework.database.RegularConstants;

public class DHTMLXMenuUtility {
	StringBuffer buffer = null;

	public DHTMLXMenuUtility() {
		buffer = new StringBuffer(2048);
	}

	public void init() {
		buffer.append("<?xml version='1.0' encoding='UTF-8'?>");
		buffer.append("<menu>");
	}

	public void finish() {
		buffer.append("</menu>");
	}

	public void startEndItem(String id, String text) {
		buffer.append("<item ");
		buffer.append(" id='").append(id).append("'");
		buffer.append(" text='").append(text).append("'");
		buffer.append(" />");
	}

	public void startItem(String id, String text) {
		buffer.append("<item ");
		buffer.append(" id='").append(id).append("'");
		buffer.append(" text='").append(text).append("'");
		buffer.append(" >");
	}

	public void startItem(String id, String text, String type) {
		buffer.append("<item ");
		buffer.append(" id='").append(id).append("'");
		buffer.append(" text='").append(text).append("'");
		buffer.append(" type='").append(type).append("'");
		buffer.append(" >");
	}

	public void startItem(String id, String text, String type, String group, String check) {
		buffer.append("<item ");
		buffer.append(" id='").append(id).append("'");
		buffer.append(" text='").append(text).append("'");
		buffer.append(" type='").append(type).append("'");
		buffer.append(" group='").append(group).append("'");
		buffer.append(" check='").append(check).append("'");
		buffer.append(" >");
	}

	public void startItem(String id, String text, String key, String value) {
		buffer.append("<item ");
		buffer.append(" id='").append(id).append("'");
		buffer.append(" text='").append(text).append("'");
		buffer.append(" ").append(key).append("='").append(value).append("'");
		buffer.append(" >");
	}

	public void startItem(String id, String text, Map<String, String> userData) {
		buffer.append("<item ");
		buffer.append("< id='").append(id).append("' ");
		buffer.append(" text='").append(text).append("'");
		for (String key : userData.keySet()) {
			buffer.append(" ").append(key).append("='").append(userData.get(key)).append("' ");
		}
		buffer.append(" >");
	}

	public void endItem() {
		buffer.append("</item>");
	}

	public void addHotKey(String key) {
		buffer.append("<hotkey ");
		buffer.append(" >");
		buffer.append(encodeData(key));
		buffer.append("</hotkey>");
	}

	private String encodeData(String data) {
		if (data == null)
			return RegularConstants.EMPTY_STRING;
		return data.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;");
	}

	public String getXML() {
		buffer.trimToSize();
		return buffer.toString();
	}
}
