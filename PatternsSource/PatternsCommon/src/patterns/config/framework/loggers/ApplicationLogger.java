package patterns.config.framework.loggers;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import patterns.config.framework.ConfigurationManager;

public class ApplicationLogger {

	static {
		Properties properties = new Properties();
		try {
			properties.load(ApplicationLogger.class.getResourceAsStream(ConfigurationManager.getLoggerConfiguration()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		PropertyConfigurator.configure(properties);

	}

	private Logger logger;
	private static ApplicationLogger instance;

	private String loggerName = null;

	private ApplicationLogger(String loggerName) {
		this.loggerName = loggerName;
		logger = Logger.getLogger("applog");
	}

	public static ApplicationLogger getInstance(String loggerName) {
		instance = new ApplicationLogger(loggerName);
		return instance;
	}

	private Logger getLogger() {
		return logger;
	}

	public void logFatal(String message) {
		getLogger().fatal("[" + loggerName + "] " + message);
	}

	public void logError(String message) {
		getLogger().error("[" + loggerName + "] " + message);
	}

	public void logDebug(String message) {
		getLogger().debug("[" + loggerName + "] " + message);
	}

	public void logInfo(String message) {
		getLogger().info("[" + loggerName + "] " + message);
	}
}