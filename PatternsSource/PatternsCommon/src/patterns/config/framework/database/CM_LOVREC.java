package patterns.config.framework.database;

public class CM_LOVREC {

	public static final String COMMON_BHOPT_1 = "1";
	public static final String COMMON_BHOPT_2 = "2";

	public static final String COMMON_CLIENTTYPE_I = "I";
	public static final String COMMON_CLIENTTYPE_J = "J";
	public static final String COMMON_CLIENTTYPE_C = "C";

	public static final String COMMON_ROLES_1 = "1";
	public static final String COMMON_ROLES_2 = "2";
	public static final String COMMON_ROLES_3 = "3";
	public static final String COMMON_ROLES_4 = "4";
	public static final String COMMON_ROLES_5 = "5";
	public static final String COMMON_ROLES_6 = "6";

	public static final String COMMON_SERVICECATEGORY_1 = "1";
	public static final String COMMON_SERVICECATEGORY_2 = "2";
	public static final String COMMON_SERVICECATEGORY_3 = "3";

	public static final String COMMON_JOBCATEGORY_1 = "1";
	public static final String COMMON_JOBCATEGORY_2 = "2";
	public static final String COMMON_JOBCATEGORY_3 = "3";

	public static final String COMMON_TFA_T = "T";
	public static final String COMMON_TFA_D = "D";

	public static final String COMMON_FILEDUPLICATE_0 = "0";
	public static final String COMMON_FILEDUPLICATE_1 = "1";
	public static final String COMMON_FILEDUPLICATE_2 = "2";

	public static final String COMMON_TYPEOFSERVICE_1 = "1";
	public static final String COMMON_TYPEOFSERVICE_2 = "2";
	public static final String COMMON_TYPEOFSERVICE_3 = "3";
	public static final String COMMON_TYPEOFSERVICE_4 = "4";
	public static final String COMMON_TYPEOFSERVICE_5 = "5";
	public static final String COMMON_TYPEOFSERVICE_6 = "6";

	public static final String COMMON_PWDDELIVERY_E = "E";
	public static final String COMMON_PWDDELIVERY_S = "S";
	public static final String COMMON_PWDDELIVERY_P = "P";

	public static final String COMMON_USERSTATUS_A = "A";
	public static final String COMMON_USERSTATUS_L = "L";
	public static final String COMMON_USERSTATUS_B = "B";
	public static final String COMMON_USERSTATUS_D = "D";
	public static final String COMMON_USERSTATUS_E = "E";

	public static final String COMMON_VISIBILITY_A = "A";
	public static final String COMMON_VISIBILITY_B = "B";

	public static final String COMMON_PINTYPE_L = "L";
	public static final String COMMON_PINTYPE_T = "T";

	public static final String COMMON_PYMTTYPE_1 = "1";
	public static final String COMMON_PYMTTYPE_2 = "2";

	public static final String COMMON_ROLEACTION_1 = "1";
	public static final String COMMON_ROLEACTION_2 = "2";
	public static final String COMMON_ROLEACTION_3 = "3";
	public static final String COMMON_ROLEACTION_4 = "4";
	public static final String COMMON_ROLEACTION_5 = "5";

	public static final String CUSTOMER_COLLECTIONOPTION_1 = "1";
	public static final String CUSTOMER_COLLECTIONOPTION_2 = "2";
	public static final String CUSTOMER_COLLECTIONOPTION_3 = "3";
	public static final String CUSTOMER_COLLECTIONOPTION_4 = "4";

	public static final String CUSTOMER_STPCHQTYPE_S = "S";
	public static final String CUSTOMER_STPCHQTYPE_M = "M";

	public static final String CUSTOMER_BALCHECK_S = "S";
	public static final String CUSTOMER_BALCHECK_W = "W";
	
	public static final String QINVBULKAUTHSTATUS_INV_PROCESS_STATUS_T = "T";
	public static final String QINVBULKAUTHSTATUS_INV_PROCESS_STATUS_U = "U";
	public static final String QINVBULKAUTHSTATUS_INV_PROCESS_STATUS_S = "S";
	public static final String QINVBULKAUTHSTATUS_INV_PROCESS_STATUS_F = "F";
	
	public static final String EINVOICEAUTH_JOB_STATUS_T = "T";
	public static final String EINVOICEAUTH_JOB_STATUS_U = "U";
	public static final String EINVOICEAUTH_JOB_STATUS_S = "S";
	public static final String EINVOICEAUTH_JOB_STATUS_F = "F";
	
	public static final String EINVBULKRPT_JOB_STATUS_T = "T";
	public static final String EINVBULKRPT_JOB_STATUS_U = "U";
	public static final String EINVBULKRPT_JOB_STATUS_S = "S";
	public static final String EINVBULKRPT_JOB_STATUS_F = "F";

	

}