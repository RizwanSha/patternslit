package patterns.config.framework.database.objects;

import java.util.Map;

import patterns.config.framework.database.BindParameterType;

public class ColumnInfo {

	private String[] columnNames;
	
	private String[] columnComments;

	private BindParameterType[] columnTypes;

	private String allColumnsString;

	private Map<String, BindParameterType> columnMapping;

	public String[] getColumnNames() {
		return columnNames;
	}

	public void setColumnNames(String[] columnNames) {
		this.columnNames = columnNames;
	}
	
	public String[] getColumnComments() {
		return columnComments;
	}

	public void setColumnComments(String[] columnComments) {
		this.columnComments = columnComments;
	}
	public BindParameterType[] getColumnTypes() {
		return columnTypes;
	}

	public void setColumnTypes(BindParameterType[] columnTypes) {
		this.columnTypes = columnTypes;
	}

	public String getAllColumnsString() {
		return allColumnsString;
	}

	public void setAllColumnsString(String allColumnsString) {
		this.allColumnsString = allColumnsString;
	}

	@Override
	public String toString() {
		return allColumnsString;
	}

	public void setColumnMapping(Map<String, BindParameterType> columnMapping) {
		this.columnMapping = columnMapping;
	}

	public Map<String, BindParameterType> getColumnMapping() {
		return columnMapping;
	}
}
