package patterns.config.framework.database.objects;

import java.util.Map;

import patterns.config.framework.database.BindParameterType;

public class PrimaryKeyInfo {

	private String[] columnNames;
	private BindParameterType[] columnTypes;
	private String whereClause;
	private Map<String, BindParameterType> columnMapping;

	public String[] getColumnNames() {
		return columnNames;
	}

	public void setColumnNames(String[] columnNames) {
		this.columnNames = columnNames;
	}

	public BindParameterType[] getColumnTypes() {
		return columnTypes;
	}

	public void setColumnTypes(BindParameterType[] columnTypes) {
		this.columnTypes = columnTypes;
	}

	public void setWhereClause(String whereClause) {
		this.whereClause = whereClause;
	}

	public String getWhereClause() {
		return whereClause;
	}

	@Override
	public String toString() {
		return whereClause;
	}

	public void setColumnMapping(Map<String, BindParameterType> columnMapping) {
		this.columnMapping = columnMapping;
	}

	public Map<String, BindParameterType> getColumnMapping() {
		return columnMapping;
	}

}