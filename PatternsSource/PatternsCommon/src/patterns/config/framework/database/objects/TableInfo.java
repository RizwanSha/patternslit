package patterns.config.framework.database.objects;

import java.util.Set;

public class TableInfo {

	private ColumnInfo columnInfo;
	private PrimaryKeyInfo primaryKeyInfo;
	private Set<String> noUpdationColumnInfo;
	

	public void setColumnInfo(ColumnInfo columnInfo) {
		this.columnInfo = columnInfo;
	}

	public ColumnInfo getColumnInfo() {
		return columnInfo;
	}

	public void setPrimaryKeyInfo(PrimaryKeyInfo primaryKeyInfo) {
		this.primaryKeyInfo = primaryKeyInfo;
	}

	public PrimaryKeyInfo getPrimaryKeyInfo() {
		return primaryKeyInfo;
	}

	@Override
	public String toString() {
		return getColumnInfo() + " @@@@ " + getPrimaryKeyInfo();
	}

	public void setNoUpdationColumnInfo(Set<String> noUpdationColumnInfo) {
		this.noUpdationColumnInfo = noUpdationColumnInfo;
	}

	public Set<String> getNoUpdationColumnInfo() {
		return noUpdationColumnInfo;
	}
}
