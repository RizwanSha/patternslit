package patterns.config.framework.database;

/**
 * @author PATTERNS0020
 * 
 */
public final class FileConstants {

	public static final int STEP_PROC_BEGIN = 1;
	public static final int STEP_FILE_VALIDATION = 2;
	public static final int STEP_OPEN_BATCH = 3;
	public static final int STEP_PREPARE_SPLIT_MANAGER = 4;
	public static final int STEP_PROCESS_DEBIT_SPLITS = 5;
	public static final int STEP_PROCESS_CREDIT_SPLITS = 6;
	public static final int STEP_VALIDATE_RECORDS = 7;
	public static final int STEP_UPDATE_DEBIT_LEGS = 8;
	public static final int STEP_UPDATE_CREDIT_LEGS = 9;
	public static final int STEP_POST_DEBITS = 10;
	public static final int STEP_POST_CREDITS = 11;
	public static final int STEP_CLOSE_BATCH = 12;
	public static final int STEP_RESPONSE_FILE_GEN = 13;
	public static final int STEP_END = 14;
	public static final int STEP_ERROR = 15;
	public static final int STEP_SPLIT_UPDATES = 16;

	public enum StepStage {
		BEGIN, END
	}

	public static final String STEP_STAGE_KEY = "STEP_STAGE_KEY";
	public static final String SPLIT_INFO_KEY = "SPLIT_INFO_KEY";
	public static final String SPLIT_COUNT_KEY = "SPLIT_COUNT_KEY";
	public static final String SPLIT_THRESHOLD_KEY = "SPLIT_THRESHOLD_KEY";
	public static final String SPLIT_ID_KEY = "SPLIT_ID_KEY";
	public static final String SPLIT_STAGE_KEY = "SPLIT_STAGE_KEY";

	public static final int SPLIT_STAGE_VALIDATION = 1;
	public static final int SPLIT_STAGE_UPDATE_LEGS = 2;
	public static final int SPLIT_STAGE_POST_EFFECTS = 3;

	public static final String PROCESS_SUCCESS = "S";
	public static final String PROCESS_FAILURE = "F";
	public static final String PROCESS_PENDING = "P";
	public static final String PROCESS_QUEUED = "Q";
	public static final String PROCESS_PROGRESS = "I";

	public static final String STATUS_PROC_BEGIN = "01";
	public static final String STATUS_VALIDATE_FILE = "02";
	public static final String STATUS_OPEN_BATCH = "03";
	public static final String STATUS_PREPARE_SPLIT = "04";
	public static final String STATUS_PROCESS_DEBIT_SPLIT = "05";
	public static final String STATUS_PROCESS_CREDIT_SPLIT = "06";
	public static final String STATUS_VALIDATE_RECORDS = "07";
	public static final String STATUS_UPDATE_DEBIT_LEGS = "08";
	public static final String STATUS_UPDATE_CREDIT_LEGS = "09";
	public static final String STATUS_POST_DEBITS = "10";
	public static final String STATUS_POST_CREDITS = "11";
	public static final String STATUS_CLOSE_BATCH = "12";
	public static final String STATUS_RESPONSE_FILE_GENERATION = "13";
	public static final String STATUS_PROC_END = "14";
	

	public static final String ENTITY_CODE = "ENITY_CODE";
	public static final String SOURCE_KEY = "SOURCE_KEY";
	public static final String RUN_SL_KEY = "RUN_SL_KEY";
	public static final String VALIDATION_RESULTS_KEY = "VALIDATION_RESULTS_KEY";
	public static final String REQUEST_TYPE_KEY = "REQUEST_TYPE_KEY";

}
