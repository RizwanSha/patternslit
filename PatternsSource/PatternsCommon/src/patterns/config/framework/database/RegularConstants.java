package patterns.config.framework.database;

public class RegularConstants {

	public static final String PROCESS_METHOD = "processTBAUpdate";
	public static final String PROCESS_EFFTDATE_METHOD = "processEffDateTBAUpdate";
	public static final String INIT_DEPLOYMENT_ENTITY = "INIT_DEPLOYMENT_ENTITY";
	public static final String INIT_DEPLOYMENT_PARTITION = "INIT_DEPLOYMENT_PARTITION";
	public static final String INIT_HTTPS = "INIT_HTTPS";
	public static final String INIT_JOBS = "INIT_JOBS";
	public static final String INIT_FILEUPLOAD_DELAY = "INIT_FILEUPLOAD_DELAY";
	public static final String INIT_ACCESS_TYPE = "INIT_ACCESS_TYPE";
	public static final String INIT_ADMIN_TYPE = "INIT_ADMIN_TYPE";
	public static final String INIT_SYSTEM_CODE = "INIT_SYSTEM_CODE";

	public static final String INIT_ACCESS_TYPE_INTERNAL = "I";
	public static final String INIT_ACCESS_TYPE_EXTERNAL = "E";

	public static final String NONCE_COOKIE = "LSNONCEID";
	public static final String SESSIONID_COOKIE = "JSESSIONID";
	public static final String FORCED_LOGOUT = "FLO";
	public static final String MANUAL_LOGOUT = "MLO";
	public static final String AUTO_LOGOUT = "ALO";
	public static final String UNAUTH_LOGOUT = "ULO";

	public static final String PROCESSBO_CLASS = "_BO_CLASS";
	public static final String REF_SEPARATOR = " / ";
	public static final String PK_SEPARATOR = "|";
	public static final String TIME_SEPARATOR = ":";
	public static final String TIME_FORMAT = "%H:%i:%S";
	public static final String COLUMN_ENABLE = "1";
	public static final String COLUMN_DISABLE = "0";
	public static final String EMPTY_STRING = "";
	public static final String NULL = null;
	public static final String ZERO = "0";
	public static final String ONE = "1";
	public static final String SP_SUCCESS = "S";
	public static final String SP_FAILURE = "F";
	public static final String SUCCESS = "S";
	public static final String FAILURE = "F";
	public static final String UNDER_PROCESS = "U";
	public static final String STATUS = "STATUS";
	public static final String ERROR = "ERROR";
	public static final String SINGLE_SPACE = " ";
	public static int ADDR_INV_NUM = 1;
	public static int PHONE_INV_NUM = 2;
	public static int DESCN_INV_NUM = 3;
	public static int GROUP_INV_NUM = 4;
	public static int DOC_INV_NUM = 5;
	public static int MBILLHEAD_INV_NUM = 5;

	public static final String ADDITIONAL_INFO = "ADDITIONAL_INFO";

	public static String ORACLE_DATA_SOURCE = "1";
	public static String MYSQL_DATA_SOURCE = "2";
	public static String DATE_SEPARATOR = "-";
	//ARIBALA CHANGED ON 10/06/2015 FOR MENU CHANGE BEGIN
	public static final String SLIDE_COOKIE = "SLIDE_MODE";
	//ARIBALA ADDED END
	public static final String COLLECTION_POSTPROCESS_METHOD = "execute";
	public static final String MIN_TIME = "00:00";
	public static final String MAX_TIME = "23:59";
	public static final String RECEIVED = "R";
	public static final String PAID = "P";
	public static final String DEFAULT_STATIONERY_TYPE = "P";
	public static final String EMPTY_STATIONERY_TYPE = "E";
	public static final String PRE_PRINTED_STATIONERY_TYPE = "P";
	public static final String MAX_AGE_YEARS="120";
	public static final String MAX_AGE_MONTHS="12";
	public static final String MAX_AGE_DAYS="30";
	public static final String DAYS="days";
	public static final String YEARS="years";
	public static final String MONTHS="months";
	public static final String BAR_TYPE_OP = "OB";
	public static final String BAR_TYPE_LAB = "LR";
	public static final String BAR_TYPE_IP = "IP";
	
	public static final String INIT_MOBILE_DATE_FORMAT = "INIT_MOBILE_DATE_FORMAT";

	/** The Constant INIT_INTERFACE_DATE_FORMAT. */
	public static final String INIT_INTERFACE_DATE_FORMAT = "INIT_INTERFACE_DATE_FORMAT";
	public static final String DEBIT  = "1";
	public static final String CREDIT = "2";
	public static final String WITHDRAW  = "1";
	public static final String DEPOSIT = "2";
	public static final String AUTHORIZED = "A";
	public static final String REJECTED = "R";
}
