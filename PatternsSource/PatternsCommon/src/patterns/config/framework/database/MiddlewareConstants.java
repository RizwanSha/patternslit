/*
 * Copyright (c) 2014, 2015, Patterns Software Design Institute. All rights reserved. Unauthorized copying of this file, via any medium is strictly prohibited.
 * PATTERNS SDI PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package patterns.config.framework.database;

/**
 * The Class MiddlewareConstants contains the domain data type length values,
 * constants for mobile application related LOVs
 */
public class MiddlewareConstants {

	/** The Constant NAME_LENGTH. */
	public static final int NAME_LENGTH = 100;

	/** The Constant ADDRESS_LENGTH. */
	public static final int ADDRESS_LENGTH = 200;

	/** The Constant ACCOUNT_NUMBER_LENGTH. */
	public static final int ACCOUNT_NUMBER_LENGTH = 50;

	/** The Constant SEC_ANSWER_LENGTH. */
	public static final int SEC_ANSWER_LENGTH = 50;

	/** The Constant PID_VALUE_LENGTH. */
	public static final int PID_VALUE_LENGTH = 50;

	/** The Constant CIR_NUMBER_LENGTH. */
	public static final int CIR_NUMBER_LENGTH = 20;

	/** The Constant RUID_NUMBER_LENGTH. */
	public static final int RUID_NUMBER_LENGTH = 20;

	/** The Constant SUBJECT_COMMERCIAL. */
	public static final String SUBJECT_COMMERCIAL = "COMM";

	/** The Constant SUBJECT_SELF. */
	public static final String SUBJECT_SELF = "CONS";

	/** The Constant BANK_APP. */
	public static final String BANK_APP = "BANK";

	/** The Constant SELF_APP. */
	public static final String SELF_APP = "SELF";

	/** The Constant OTP_DISPATCH. */
	public static final String OTP_DISPATCH = "OTPDISP";

	/** The Constant CIR_DISPATCH. */
	public static final String CIR_DISPATCH = "CIRDISP";

	/** The Constant ACTLINK_DISPATCH. */
	public static final String ACTLINK_DISPATCH = "ACTLINKDISP";

	/** The Constant SUCCESS. */
	public static final String SUCCESS = "SUCCESS";

	/** The Constant FAILURE. */
	public static final String FAILURE = "FAILURE";

	/** The Constant SMS_DISPATCH. */
	public static final String SMS_DISPATCH = "SMS";

	/** The Constant EMAIL_DISPATCH. */
	public static final String EMAIL_DISPATCH = "EMAIL";

	/** The Constant REQUEST_CIR. */
	public static final String REQUEST_CIR = "2";

	/** The Constant REQUEST_RUID. */
	public static final String REQUEST_RUID = "1";

	/** The Constant XML_REQUEST. */
	public static final String XML_REQUEST = "1";

	/** The Constant PDF_REQUEST. */
	public static final String PDF_REQUEST = "2";

	public static final String REQ_TYPE_LOGIN = "LOGIN";

	public static final String REQ_TYPE_INIT = "INIT";

	public static final String REQ_TYPE_QUERY = "QUERY";

	public static final String REQ_TYPE_SUBMIT = "SUBMIT";

	public static final String QUERY_TYPE_INIT = "INIT";

	public static final String QUERY_TYPE_UPDATE = "UPDATE";

	public static final String QUERY_TYPE_FETCH = "FETCH";

	public static final String QUERY_TYPE_GRIDQUERY = "GRIDQUERY";

	public static final String QUERY_TYPE_LOOKUP = "LOOKUP";

	public static final String LOGIN_TYPE_INIT = "INIT";

	public static final String LOGIN_TYPE_AUTH = "AUTH";
	public static final String DOC_FOR_PHOTO = "1";
	public static final String DOC_FOR_SIGNATURE = "2";

	public static final String REQ_TYPE_LOOKUP = "LOOKUP";

	public static final String WALLET_STATUS_NORMAL = "00";
	public static final String WALLET_STATUS_TRFBNF = "01";
	public static final String WALLET_STATUS_PENDING = "02";
	public static final String WALLET_STATUS_DISBURSED = "03";
	public static final String WALLET_STATUS_RETURNED = "05";
	
	public static final String WALLET_STATUS_NORMAL_LBL = "Normal";
	public static final String WALLET_STATUS_TRFBNF_LBL = "Transferred, but not funded";
	public static final String WALLET_STATUS_PENDING_LBL = "Pending";
	public static final String WALLET_STATUS_DISBURSED_LBL = "Disbursed";
	public static final String WALLET_STATUS_RETURNED_LBL = "Returned";
	

}
