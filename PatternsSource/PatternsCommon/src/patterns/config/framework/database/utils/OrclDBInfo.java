package patterns.config.framework.database.utils;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import patterns.config.framework.database.BindParameterType;
import patterns.config.framework.database.objects.ColumnInfo;
import patterns.config.framework.database.objects.PrimaryKeyInfo;
import patterns.config.framework.database.objects.TableInfo;

public class OrclDBInfo  extends DBInfo{

	public OrclDBInfo() {
	}

	private static String primaryKeyQuery = "SELECT A.COLUMN_NAME,(SELECT C.COLTYPE FROM COL C WHERE C.TNAME = ? AND C.CNAME = A.COLUMN_NAME AND C.COLNO = A.POSITION) COLTYPE FROM USER_CONS_COLUMNS A WHERE A.OWNER = USER AND A.TABLE_NAME = ? AND A.CONSTRAINT_NAME = (SELECT B.CONSTRAINT_NAME FROM USER_CONSTRAINTS B WHERE B.OWNER = USER AND B.TABLE_NAME = ? AND B.CONSTRAINT_TYPE = 'P') ORDER BY A.POSITION ASC ";
	private static String primaryKeyCountQuery = "SELECT COUNT(*) FROM USER_CONS_COLUMNS A WHERE A.OWNER = USER AND A.TABLE_NAME = ? AND A.CONSTRAINT_NAME = (SELECT B.CONSTRAINT_NAME FROM USER_CONSTRAINTS B WHERE B.OWNER = USER AND B.TABLE_NAME = ? AND B.CONSTRAINT_TYPE = 'P')";
	private static String columnQuery = "SELECT C.CNAME,C.COLTYPE,C.COLNO from COL C where C.TNAME=? ORDER BY C.COLNO ASC";
	private static String columnCountQuery = "SELECT COUNT(*) from COL C where C.TNAME=?";
	private static String noUpdationColumnInfoQuery = "SELECT C.CMNAUTH_COLUMN_NAME from CMNAUTHCOL C where C.CMNAUTH_TABLE_NAME=?";

	public TableInfo getTableInfo(String tableName) {
		DBContext dbContext = new DBContext();
		TableInfo tableInfo = new TableInfo();
		ColumnInfo columnInfo = new ColumnInfo();
		PrimaryKeyInfo primaryKeyInfo = new PrimaryKeyInfo();
		int columnCount = 0;
		int primaryKeyCount = 0;
		String[] columns;
		BindParameterType[] columnTypes;
		String[] primaryKeyColumns;
		BindParameterType[] primaryKeyColumnTypes;
		Map<String, BindParameterType> columnType = new HashMap<String, BindParameterType>();
		Map<String, BindParameterType> keyColumnType = new HashMap<String, BindParameterType>();

		try {
			DBUtil util = dbContext.createUtilInstance();
			util.reset();
			util.setSql(columnCountQuery);
			util.setString(1, tableName);
			ResultSet rset = util.executeQuery();
			while (rset.next()) {
				columnCount = rset.getInt(1);
			}
			util.reset();
			columns = new String[columnCount];
			columnTypes = new BindParameterType[columnCount];
			util.setSql(columnQuery);
			util.setString(1, tableName);
			rset = util.executeQuery();
			int i = 0;
			StringBuffer allColumnsString = new StringBuffer();
			while (rset.next()) {
				columns[i] = rset.getString(1);
				columnTypes[i] = decodeType(rset.getString(2));
				allColumnsString.append(rset.getString(1));
				if (i < columnCount - 1)
					allColumnsString.append(",");
				columnType.put(columns[i], columnTypes[i]);
				++i;
			}
			util.reset();
			columnInfo.setColumnNames(columns);
			columnInfo.setColumnTypes(columnTypes);
			columnInfo.setColumnMapping(columnType);
			columnInfo.setAllColumnsString(allColumnsString.toString());
			tableInfo.setColumnInfo(columnInfo);
			util.setSql(primaryKeyCountQuery);
			util.setString(1, tableName);
			util.setString(2, tableName);
			rset = util.executeQuery();
			while (rset.next()) {
				primaryKeyCount = rset.getInt(1);
			}
			util.reset();
			primaryKeyColumns = new String[primaryKeyCount];
			primaryKeyColumnTypes = new BindParameterType[primaryKeyCount];
			util.setSql(primaryKeyQuery);
			util.setString(1, tableName);
			util.setString(2, tableName);
			util.setString(3, tableName);
			rset = util.executeQuery();
			i = 0;
			StringBuffer whereClauseString = new StringBuffer();
			while (rset.next()) {
				primaryKeyColumns[i] = rset.getString(1);
				primaryKeyColumnTypes[i] = columnType.get(rset.getString(1));
				whereClauseString.append(rset.getString(1)).append(" = ? ");
				if (i < primaryKeyCount - 1)
					whereClauseString.append(" AND ");
				keyColumnType.put(primaryKeyColumns[i], primaryKeyColumnTypes[i]);
				++i;
			}
			util.reset();
			primaryKeyInfo.setColumnNames(primaryKeyColumns);
			primaryKeyInfo.setColumnTypes(primaryKeyColumnTypes);
			primaryKeyInfo.setWhereClause(whereClauseString.toString());
			tableInfo.setPrimaryKeyInfo(primaryKeyInfo);

			util.reset();
			util.setSql(noUpdationColumnInfoQuery);
			util.setString(1, tableName);
			rset = util.executeQuery();
			Set<String> noUpdationColumnInfo = new HashSet<String>();
			while (rset.next()) {
				noUpdationColumnInfo.add(rset.getString(1));
			}
			util.reset();
			tableInfo.setNoUpdationColumnInfo(noUpdationColumnInfo);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return tableInfo;
	}

	private static BindParameterType decodeType(String typeName) {
		if (typeName.equals("VARCHAR2"))
			return BindParameterType.VARCHAR;
		else if (typeName.equals("CHAR"))
			return BindParameterType.VARCHAR;
		else if (typeName.equals("DATE"))
			return BindParameterType.DATE;
		else if (typeName.equals("NUMBER"))
			return BindParameterType.INTEGER;
		else if (typeName.equals("LONG"))
			return BindParameterType.LONG;
		else if (typeName.equals("BIGDECIMAL"))
			return BindParameterType.BIGDECIMAL;
		else if (typeName.equals("BIGINT"))
			return BindParameterType.BIGINT;
		else if (typeName.equals("TIMESTAMP"))
			return BindParameterType.TIMESTAMP;
		return BindParameterType.VARCHAR;
	}
}