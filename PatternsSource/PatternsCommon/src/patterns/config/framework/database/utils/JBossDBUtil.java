package patterns.config.framework.database.utils;

import java.io.StringReader;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.SQLException;

public class JBossDBUtil extends DBUtil {

	public JBossDBUtil() {
	}

	public void setClob(int index, String value) throws SQLException {
		prep.setClob(index, new StringReader(value));
	}

	public Clob getClob(int index) throws SQLException {
		return ((CallableStatement) prep).getClob(index);
	}
}