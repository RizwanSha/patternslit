package patterns.config.framework.database.utils;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public abstract class DBUtil {

	public static final boolean PREPARED = true;
	public static final boolean CALLABLE = false;

	protected Connection conn;
	protected PreparedStatement prep;
	protected ResultSet rset;
	private boolean mode = PREPARED;
	protected DBContext context = null;

	protected DBUtil() {
		prep = null;
		rset = null;
		mode = PREPARED;
	}

	protected void setContext(DBContext context) {
		this.context = context;
	}

	public abstract void setClob(int index, String value) throws SQLException;

	public abstract Clob getClob(int index) throws SQLException;

	public void setCharacterStream(int index, Reader reader, long length) throws SQLException {
		prep.setCharacterStream(index, reader, length);
	}

	public void setBinaryStream(int index, InputStream is, long length) throws SQLException {
		prep.setBinaryStream(index, is, length);
	}
	
	public void setBinaryStream(int index, InputStream is) throws SQLException {
		prep.setBinaryStream(index, is);
	}

	public void setBlob(int index, Blob value) throws SQLException {
		prep.setBlob(index, value);
	}

	public Blob getBlob(int index) throws SQLException {
		return ((CallableStatement) prep).getBlob(index);
	}

	public void setInt(int index, int value) throws SQLException {
		prep.setInt(index, value);
	}

	public int getInt(int index) throws SQLException {
		return ((CallableStatement) prep).getInt(index);
	}

	public void setLong(int index, long value) throws SQLException {
		prep.setLong(index, value);
	}

	public long getLong(int index) throws SQLException {
		return ((CallableStatement) prep).getLong(index);
	}

	public void setBigDecimal(int index, BigDecimal value) throws SQLException {
		prep.setBigDecimal(index, value);
	}

	public BigDecimal getBigDecimal(int index) throws SQLException {
		return ((CallableStatement) prep).getBigDecimal(index);
	}

	public void setDouble(int index, double value) throws SQLException {
		prep.setDouble(index, value);
	}

	public double getDouble(int index) throws SQLException {
		return ((CallableStatement) prep).getDouble(index);
	}

	public void setFloat(int index, float value) throws SQLException {
		prep.setFloat(index, value);
	}

	public float getFloat(int index) throws SQLException {
		return ((CallableStatement) prep).getFloat(index);
	}

	public void setString(int index, String value) throws SQLException {
		prep.setString(index, value);
	}

	public String getString(int index) throws SQLException {
		return ((CallableStatement) prep).getString(index);
	}

	public void setDate(int index, Date value) throws SQLException {
		prep.setDate(index, value);
	}

	public Date getDate(int index) throws SQLException {
		return ((CallableStatement) prep).getDate(index);
	}

	public void setBytes(int index, byte[] value) throws SQLException {
		prep.setBytes(index, value);
	}

	public byte[] getBytes(int index) throws SQLException {
		return ((CallableStatement) prep).getBytes(index);
	}

	public void setBoolean(int index, boolean value) throws SQLException {
		prep.setBoolean(index, value);
	}

	public boolean getBoolean(int index) throws SQLException {
		return ((CallableStatement) prep).getBoolean(index);
	}

	public void setNull(int index, int type) throws SQLException {
		prep.setNull(index, type);
	}

	public void setTimestamp(int index, java.sql.Timestamp time) throws SQLException {
		prep.setTimestamp(index, time);
	}

	public Timestamp getTimestamp(int index) throws SQLException {
		return ((CallableStatement) prep).getTimestamp(index);
	}

	public void registerOutParameter(int index, int type) throws SQLException {
		((CallableStatement) prep).registerOutParameter(index, type);
	}

	public int[] executeBatch() throws SQLException {
		return prep.executeBatch();
	}

	public void clearBatch() throws SQLException {
		prep.clearBatch();
	}

	public void addBatch() throws SQLException {
		prep.addBatch();
	}

	public ResultSet executeQuery() throws SQLException {
		rset = prep.executeQuery();
		return (rset);
	}

	public int executeUpdate() throws SQLException {
		return (prep.executeUpdate());
	}

	public boolean execute() throws SQLException {
		return (prep.execute());
	}

	public void setMode(boolean mode) {
		this.mode = mode;
	}

	public void reset() {
		subclose();
		setMode(PREPARED);
	}

	public void setSql(String sql) throws SQLException {
		if (conn == null) {
			conn = context.openConnection();
		}
		if (mode == PREPARED) {
			prep = conn.prepareStatement(sql,ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
		} else {
			prep = conn.prepareCall(sql);
		}
	}
	


	private void subclose() {
		if (rset != null) {
			try {
				rset.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			rset = null;
		}
		if (prep != null) {
			try {
				prep.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			prep = null;
		}
	}
}