package patterns.config.framework.database.utils;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.Vector;

import patterns.config.framework.DatasourceConfigurationManager;
import patterns.config.framework.service.ServiceLocator;

public final class DBContext implements AutoCloseable {
	private boolean autoCommit;
	private boolean explicitSetting = false;

	private Connection conn = null;
	private Vector<DBUtil> utilList = new Vector<DBUtil>();

	public DBContext() {
	}

	public final Connection openConnection() throws SQLException {
		if (conn != null) {
			return conn;
		}
		conn = ServiceLocator.getInstance().getDataSource(DatasourceConfigurationManager.getDataSourceJNDIName()).getConnection();
		if (explicitSetting) {
			if (autoCommit)
				conn.setAutoCommit(true);
			else
				conn.setAutoCommit(false);
		}
		return conn;
	}

	protected final void closeConnection() throws SQLException {
		if (conn != null) {
			conn.close();
		}
	}

	@Override
	public final void close() {
		for (DBUtil util : utilList) {
			util.reset();
		}
		utilList.clear();
		try {
			closeConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public DBUtil createUtilInstance() {
		DBUtil util = getDBUtil(this);
		utilList.add(util);
		return util;
	}

	public boolean isAutoCommit() {
		return autoCommit;
	}

	public void setAutoCommit(boolean autoCommit) throws Exception {
		this.autoCommit = autoCommit;
		explicitSetting = true;
	}

	public Savepoint setSavepoint() throws Exception {
		return conn.setSavepoint();
	}

	public Savepoint setSavepoint(String name) throws Exception {
		return conn.setSavepoint(name);
	}

	public void releaseSavepoint(Savepoint savepoint) throws Exception {
		conn.releaseSavepoint(savepoint);
	}

	public void commit() throws Exception {
		try {
			if (conn != null)
				conn.commit();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void rollback() throws Exception {
		try {
			if (conn != null)
				conn.rollback();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void rollback(Savepoint savepoint) throws Exception {
		try {
			if (conn != null)
				conn.rollback(savepoint);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public DBUtil getDBUtil(DBContext context) {
		DBUtil util = new JBossDBUtil();
		util.setContext(context);
		return util;
	}
}