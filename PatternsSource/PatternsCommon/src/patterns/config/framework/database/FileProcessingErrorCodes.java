package patterns.config.framework.database;

/**
 * @author PATTERNS0020
 * 
 */
public class FileProcessingErrorCodes {
	public static final String FILE_VALIDATION_ERROR = "FPCE01";
	public static final String BATCH_OPEN_ERROR = "FPCE02";
	public static final String CONTEXT_CREATION_ERROR = "FPCE03";
	public static final String SPLIT_PREPARATION_ERROR = "FPCE04";
	public static final String DEBIT_RECORD_VALIDATION_ERROR = "FPCE05";
	public static final String CREDIT_RECORD_VALIDATION_ERROR = "FPCE06";
	public static final String DEBIT_LEG_UPDATION_ERROR = "FPCE07";
	public static final String CREDIT_LEG_UPDATION_ERROR = "FPCE08";
	public static final String DEBIT_LEG_POSTING_ERROR = "FPCE09";
	public static final String CREDIT_LEG_POSTING_ERROR = "FPCE10";
	public static final String BATCH_CLOSE_ERROR = "FPCE11";
	public static final String RESPONSE_FILE_GENERATION_ERROR = "FPCE12";
	public static final String UNHANDLED_EXCEPTION = "FPCE99";

	public static final String COMPONENT_FORMAT_ERROR = "FPCE20";
	public static final String COMPONENT_VALIDATION_ERROR = "FPCE21";
	public static final String COMPONENT_UPDATION_ERROR = "FPCE22";
	public static final String COMPONENT_POSTING_ERROR = "FPCE23";

}