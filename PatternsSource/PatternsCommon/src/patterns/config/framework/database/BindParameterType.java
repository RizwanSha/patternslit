package patterns.config.framework.database;

public enum BindParameterType {

	VARCHAR, DATE, TIMESTAMP, INTEGER, LONG, BIGDECIMAL, CUSTOM, BIGINT,DECIMAL,DATETIME

}
