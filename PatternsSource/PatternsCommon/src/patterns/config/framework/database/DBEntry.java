package patterns.config.framework.database;

public class DBEntry {
	private BindParameterType type;
	private Object value;
	private String name;

	public DBEntry(String name, Object value, BindParameterType type) {
		this.type = type;
		this.value = value;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BindParameterType getType() {
		return type;
	}

	public void setType(BindParameterType type) {
		this.type = type;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
}