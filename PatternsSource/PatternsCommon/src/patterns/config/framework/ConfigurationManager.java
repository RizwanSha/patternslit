package patterns.config.framework;

import java.util.ResourceBundle;

public class ConfigurationManager {

	private static final ResourceBundle bundle = ResourceBundle.getBundle("patterns/framework/DeploymentContext");

	public static final String getDataSourceJNDIName() {
		return bundle.getString("jndiname.datasource");
	}

	public static final String getCommonDispatcherHomeJNDI() {
		return bundle.getString("jndiname.commondispatcher");
	}

	public static final String getProcessDispatcherHomeJNDI() {
		return bundle.getString("jndiname.processdispatcher");
	}

	public static final String getFileProcessDispatcherHomeJNDI() {
		return bundle.getString("jndiname.fileprocessdispatcher");
	}

	public static final String getCTransactionDispatcherHomeJNDI() {
		return bundle.getString("jndiname.ctransactiondispatcher");
	}

	public static final String getCTransactionFileDispatcherHomeJNDI() {
		return bundle.getString("jndiname.ctransactionfiledispatcher");
	}
	
	public static final String getCTransactionAuthDispatcherHomeJNDI() {
		return bundle.getString("jndiname.ctransactionauthdispatcher");
	}

	public static final String getMessageDispatcherHomeJNDI() {
		return bundle.getString("jndiname.messagefacade");
	}

	public static final String getLoggerConfiguration() {
		return bundle.getString("log4j.properties");
	}
}