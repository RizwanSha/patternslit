package patterns.config.framework.pki;

import java.io.Serializable;

public class ProcessTFAInfo implements Serializable {
	private static final long serialVersionUID = 5248525400859472003L;

	private String deploymentContext;
	private String userId;
	private String digitalCertificateInventoryNumber;
	private String tfaRequired;
	private String tfaSuccess;
	private String tfaValue;
	private String tfaEncodedValue;
	private String tfaInventoryNumber;

	public String getDeploymentContext() {
		return deploymentContext;
	}

	public void setDeploymentContext(String deploymentContext) {
		this.deploymentContext = deploymentContext;
	}

	public String getTfaInventoryNumber() {
		return tfaInventoryNumber;
	}

	public void setTfaInventoryNumber(String tfaInventoryNumber) {
		this.tfaInventoryNumber = tfaInventoryNumber;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDigitalCertificateInventoryNumber() {
		return digitalCertificateInventoryNumber;
	}

	public void setDigitalCertificateInventoryNumber(String digitalCertificateInventoryNumber) {
		this.digitalCertificateInventoryNumber = digitalCertificateInventoryNumber;
	}

	public String getTfaRequired() {
		return tfaRequired;
	}

	public void setTfaRequired(String tfaRequired) {
		this.tfaRequired = tfaRequired;
	}

	public String getTfaSuccess() {
		return tfaSuccess;
	}

	public void setTfaSuccess(String tfaSuccess) {
		this.tfaSuccess = tfaSuccess;
	}

	public String getTfaValue() {
		return tfaValue;
	}

	public void setTfaValue(String tfaValue) {
		this.tfaValue = tfaValue;
	}

	public String getTfaEncodedValue() {
		return tfaEncodedValue;
	}

	public void setTfaEncodedValue(String tfaEncodedValue) {
		this.tfaEncodedValue = tfaEncodedValue;
	}

}
