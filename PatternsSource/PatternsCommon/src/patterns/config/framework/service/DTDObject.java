package patterns.config.framework.service;

import java.io.Serializable;
import java.io.StringReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import patterns.config.framework.utils.Formatter;

public class DTDObject extends DefaultHandler implements Serializable {

	public DTDObject() {
	}

	private static final long serialVersionUID = 6967776575442124518L;
	private ArrayList<String> arrListColNames = new ArrayList<String>();
	private ArrayList<String[]> arrListRows = new ArrayList<String[]>();

	private int currColIndex; // Used for XML Parsing in startElement() and
								// characters()
	StringBuffer tagValueBuf = new StringBuffer();

	// Jainudeen Changes 28/NOV/2014 Begin
	private String dateFormat = null;

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	// Jainudeen Changes 28/NOV/2014 End

	public void addColumn(int ColIndex, String ColName) {
		arrListColNames.add(ColIndex, ColName);
	}

	public void addColumn(int ColIndex) {
		arrListColNames.add(ColIndex, "Cell" + ColIndex);
	}

	public void addRow() {
		int NoOfCols = arrListColNames.size();
		String[] Row = new String[NoOfCols];
		arrListRows.add(arrListRows.size(), Row);
	}

	public void clearRows() {
		arrListRows.clear();
	}

	public void clearRow(int index) {
		arrListRows.remove(index);
	}

	public void clear() {
		for (int i = 0; i < arrListColNames.size(); i++)
			arrListColNames.remove(i);
		for (int i = 0; i < arrListRows.size(); i++)
			arrListRows.remove(i);
	}

	public String getValue(int RowIndex, int ColIndex) {
		String[] arrRow = (String[]) arrListRows.get(RowIndex);
		return arrRow[ColIndex];
	}

	public void setValue(int RowIndex, int ColIndex, String Value) {
		String[] arrRow = (String[]) arrListRows.get(RowIndex);
		arrRow[ColIndex] = Value;
	}

	public String getValue(int RowIndex, String ColName) {
		return getValue(RowIndex, arrListColNames.indexOf(ColName));
	}

	// Jainudeen Changes 28/NOV/2014 Begin
	public Date getDate(int RowIndex, String ColName) {
		try {
			return Formatter.getDate(getValue(RowIndex, arrListColNames.indexOf(ColName)), getDateFormat());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	// Jainudeen Changes 28/NOV/2014 End

	public void setValue(int RowIndex, String ColName, String Value) {
		setValue(RowIndex, arrListColNames.indexOf(ColName), Value);
	}

	public int getColSize() {
		return arrListColNames.size();
	}

	public void setColSize(int colSize) {
		for (int i = 0; i < colSize; i++) {
			arrListColNames.add(i, "Cell" + i);
		}
	}

	public int getRowSize() {
		return arrListRows.size();
	}

	public String getReportXML() {
		StringBuffer buffer = new StringBuffer(getRowSize() * getColSize());
		String xml;
		buffer.append("<rows>");
		for (int i = 0; i < getRowSize(); i++) {
			buffer.append("<row id='");
			buffer.append((i + 1));
			buffer.append("'>");
			for (int j = 0; j < getColSize(); j++) {
				String colName = getColNames().get(j);
				buffer.append("<" + colName + ">");
				buffer.append(getValue(i, j));
				buffer.append("</" + colName + ">");
			}
			buffer.append("</row>");
		}
		buffer.append("</rows>");
		xml = buffer.toString().replaceAll("&", "&amp;");
		return xml;
	}

	public String getXML() {
		StringBuffer buffer = new StringBuffer(getRowSize() * getColSize());
		String xml;
		buffer.append("<rows>");
		for (int i = 0; i < getRowSize(); i++) {
			buffer.append("<row id='");
			buffer.append((i + 1));
			buffer.append("'><cell>");
			buffer.append((i + 1));
			buffer.append("</cell>");
			for (int j = 0; j < getColSize(); j++) {
				buffer.append("<cell>");
				buffer.append(getValue(i, j));
				buffer.append("</cell>");
			}
			buffer.append("</row>");
		}
		buffer.append("</rows>");
		xml = buffer.toString().replaceAll("&", "&amp;");
		return xml;
	}

	public void setXML(String xmlString) throws Exception {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();
		StringReader strreader = new StringReader(xmlString);
		InputSource insource = new InputSource(strreader);

		saxParser.parse(insource, this);

	}

	public void startElement(String namespaceURI, String sName, String qName, Attributes attrs) throws SAXException {
		tagValueBuf.setLength(0);
		if (qName.equals("row")) {
			addRow();
			currColIndex = -1;
		}

	}

	public void characters(char[] ch, int start, int length) throws SAXException {
		String tagValue = new String(ch, start, length);
		tagValueBuf.append(tagValue);
	}

	public void endElement(String namespaceURI, String sName, String qName) throws SAXException {
		if (qName.indexOf("cell") > -1) {
			String tagValue = tagValueBuf.toString();
			currColIndex = currColIndex + 1;
			setValue(arrListRows.size() - 1, currColIndex, tagValue.trim());
		}

	}

	public void warning(SAXParseException e) {
	}

	public void error(SAXParseException e) {
	}

	public String getRowValue(int rowIndex) {
		StringBuffer strRowValue = null;
		for (int colIndex = 0; colIndex < getColSize(); colIndex++) {
			if (colIndex == 0)
				strRowValue = new StringBuffer(getValue(rowIndex, colIndex));
			else
				strRowValue.append("|");
			strRowValue.append(getValue(rowIndex, colIndex));
		}
		return strRowValue.toString();
	}

	public ArrayList<String> getColNames() {
		return arrListColNames;
	}

	public int getColCount() {
		return arrListColNames.size();
	}

	public int getRowCount() {
		return arrListRows.size();
	}
}
