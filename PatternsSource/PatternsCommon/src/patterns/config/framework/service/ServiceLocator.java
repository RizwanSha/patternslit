package patterns.config.framework.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ServiceLocator {

	private static ServiceLocator serviceLocator;
	private InitialContext initialContext = null;
	private Map<String, Object> mapCache;

	static {
		serviceLocator = new ServiceLocator();
	}

	private ServiceLocator() {
		super();
		try {
			initialContext = new InitialContext();
			mapCache = Collections.synchronizedMap(new HashMap<String, Object>());
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	static public ServiceLocator getInstance() {
		return serviceLocator;
	}

	public DataSource getDataSource(String dataSourceJNDI) {
		DataSource dataSource = null;
		if (mapCache.containsKey(dataSourceJNDI)) {
			dataSource = (DataSource) mapCache.get(dataSourceJNDI);
			if (dataSource != null) {
				return dataSource;
			}
		}
		if (initialContext == null) {
			try {
				initialContext = new InitialContext();
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}
		if (initialContext != null) {
			try {
				dataSource = (DataSource) initialContext.lookup(dataSourceJNDI);
				mapCache.put(dataSourceJNDI, dataSource);
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}
		return dataSource;
	}

	public Object lookupObject(String jndiName) {
		Object localInterface = null;
		if (mapCache.containsKey(jndiName)) {
			localInterface = mapCache.get(jndiName);
			if (localInterface != null) {
				return localInterface;
			}
		}
		if (initialContext == null) {
			try {
				initialContext = new InitialContext();
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}
		if (initialContext != null) {
			try {
				localInterface = initialContext.lookup(jndiName);
				mapCache.put(jndiName, localInterface);
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}
		return localInterface;
	}

	@SuppressWarnings("unchecked")
	public Class<? extends Object> getBO(String boName) {
		Class<? extends Object> boClassReference = null;
		if (mapCache.containsKey(boName)) {
			boClassReference = (Class<? extends Object>) mapCache.get(boName);
			if (boClassReference != null) {
				return boClassReference;
			}
		}
		try {
			boClassReference = Class.forName(boName);
			mapCache.put(boName, boClassReference);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return boClassReference;
	}

	@SuppressWarnings("unchecked")
	public Class<? extends Object> getInterceptor(String interceptorName) {
		Class<? extends Object> interceptorClassReference = null;
		if (mapCache.containsKey(interceptorName)) {
			interceptorClassReference = (Class<? extends Object>) mapCache.get(interceptorName);
			if (interceptorClassReference != null) {
				return interceptorClassReference;
			}
		}
		try {
			interceptorClassReference = Class.forName(interceptorName);
			mapCache.put(interceptorName, interceptorClassReference);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return interceptorClassReference;
	}

}
