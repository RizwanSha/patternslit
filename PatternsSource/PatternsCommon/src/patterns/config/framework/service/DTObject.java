package patterns.config.framework.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import patterns.config.framework.utils.Formatter;

public class DTObject implements Serializable {

	private static final long serialVersionUID = -402685876967698341L;
	protected Map<String, Object> internalStore = Collections.synchronizedMap(new HashMap<String, Object>());
	public static final String ERROR = "error";

	private boolean error = false;

	public boolean hasError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	// Jainudeen Changes 28/NOV/2014 Begin
	private String dateFormat = null;

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	// Jainudeen Changes 28/NOV/2014 End

	public void importFrom(DTObject sourceDTO) {
		this.reset();
		this.internalStore.putAll(sourceDTO.getInternalMap());
	}

	public void exportTo(DTObject destinationDTO) {
		destinationDTO.reset();
		destinationDTO.getInternalMap().putAll(this.internalStore);
	}

	public String get(String key) {
		// Aribala added
		String result = null;
		if (internalStore.get(key) instanceof String)
			result = (String) internalStore.get(key);
		else if (internalStore.get(key) instanceof DTDObject) {
			DTDObject keyValue = (DTDObject) internalStore.get(key);
			result = keyValue.getXML();
		}
		// Aribala added
		return result;
	}

	// Jainudeen Changes 28/NOV/2014 Begin
	public Date getDate(String key) {
		try {
			return Formatter.getDate(get(key), getDateFormat());
		} catch (Exception e) {
			return null;
		}
	}

	// Jainudeen Changes 28/NOV/2014 End

	public void set(String key, String value) {
		internalStore.put(key, value);
	}

	public DTDObject getDTDObject(String KeyName) {
		return (DTDObject) internalStore.get(KeyName);
	}

	public void setDTDObject(String KeyName, DTDObject Value) {
		// Jainudeen Changes 28/NOV/2014 Begin
		Value.setDateFormat(getDateFormat());
		// Jainudeen Changes 28/NOV/2014 End
		internalStore.put(KeyName, Value);
	}

	public String getDTDObject(String KeyName, int RowIndex, int ColIndex) {
		DTDObject objDtlInfo = (DTDObject) internalStore.get(KeyName);
		return objDtlInfo.getValue(RowIndex, ColIndex);
	}

	public void setDTDObject(String KeyName, int RowIndex, int ColIndex, String Value) {
		DTDObject objDtlInfo = (DTDObject) internalStore.get(KeyName);
		if (objDtlInfo == null) {
			objDtlInfo = new DTDObject();
			internalStore.put(KeyName, objDtlInfo);
		}
		objDtlInfo.setValue(RowIndex, ColIndex, Value);
	}

	public String getDTDObject(String KeyName, int RowIndex, String ColName) {
		DTDObject objDtlInfo = (DTDObject) internalStore.get(KeyName);
		return objDtlInfo.getValue(RowIndex, ColName);
	}

	public void setDTDObject(String KeyName, int RowIndex, String ColName, String Value) {
		DTDObject objDtlInfo = (DTDObject) internalStore.get(KeyName);
		if (objDtlInfo == null) {
			objDtlInfo = new DTDObject();
			internalStore.put(KeyName, objDtlInfo);
		}
		objDtlInfo.setValue(RowIndex, ColName, Value);
	}

	public String getXMLDTDObject(String KeyName) {
		DTDObject objDtlInfo = (DTDObject) internalStore.get(KeyName);
		return objDtlInfo.getXML();
	}

	public void setXMLDTDObject(String KeyName, String XmlString) throws Exception {
		DTDObject objDtlInfo = (DTDObject) internalStore.get(KeyName);
		if (objDtlInfo == null) {
			objDtlInfo = new DTDObject();
			internalStore.put(KeyName, objDtlInfo);
		}
		objDtlInfo.setXML(XmlString);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getMap(String key) {
		return (Map<String, Object>) internalStore.get(key);
	}

	public void setMap(String key, HashMap<String, Object> value) {
		internalStore.put(key, value);
	}

	public Map<String, Object> getInternalMap() {
		return internalStore;
	}

	public boolean containsKey(String key) {
		return internalStore.containsKey(key);
	}

	public boolean containsValue(String key) {
		return internalStore.containsValue(key);
	}

	public void reset() {
		internalStore.clear();
		error = false;
	}

	public void remove(String key) {
		if (internalStore.containsKey(key))
			internalStore.remove(key);
	}

	public void setObject(String key, Object value) {
		internalStore.put(key, value);
	}

	public Object getObject(String key) {
		return internalStore.get(key);
	}

	public Object[] getObjectL(String key) {
		return (Object[]) internalStore.get(key);
	}

	public Set<String> getKeys() {
		return internalStore.keySet();
	}

	public String toXML() {
		StringBuffer buffer = new StringBuffer();
		Set<String> keyNames = getKeys();
		for (String keyName : keyNames) {
			if (!keyName.equals(ERROR)) {
				try {
					buffer.append("<" + keyName + ">");
					buffer.append("<![CDATA[");
					if (getObject(keyName) instanceof String) {
						if (get(keyName) != null) {
							buffer.append(get(keyName));
						} else {
							buffer.append("");
						}
					} else if (getObject(keyName) instanceof Timestamp) {
						String value = Formatter.getDate((java.util.Date) getObject(keyName), "dd-MM-YYYY");
						if (value != null) {
							buffer.append(value);
						} else {
							buffer.append("");
						}
					}
					buffer.append("]]>");
					buffer.append("</" + keyName + ">");
				} catch (Exception e) {
					buffer.setLength(0);
				} finally {
				}
			}
		}
		return buffer.toString();
	}

	@Override
	public String toString() {
		return internalStore.toString();
	}

	public Integer getInteger(String key) {
		Object value = internalStore.get(key);
		return (Integer) value;
	}

	public void setInteger(String key, Integer value) {
		internalStore.put(key, value);
	}
}
