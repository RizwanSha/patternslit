package patterns.config.framework.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Formatter {

	public static String getDate(String date, String sourceFormat, String targetFormat) throws ParseException {
		if (date == null) {
			return null;
		}

		SimpleDateFormat sourceFormatter = new SimpleDateFormat(sourceFormat);
		Date sourceDateInstance = sourceFormatter.parse(date);
		SimpleDateFormat targetFormatter = new SimpleDateFormat(targetFormat);
		return targetFormatter.format(sourceDateInstance);
	}

	public static String getDate(Date date, String format) {
		if (date == null)
			return null;
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}

	public static Date getDate(String date, String format) throws ParseException {
		if (date == null) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(convertSimpleFormat(format));
		return sdf.parse(date);
	}

	private static String convertSimpleFormat(String format) {
		if (format.equals("dd/MM/yyyy"))
			return "dd-MM-yyyy";
		else if (format.equals("dd-MM-yyyy"))
			return "dd-MM-yyyy";
		else if (format.equals("dd-MM-yyyy HH24:MI:SS"))
			return "dd-MM-yyyy HH:MM:SS";
		else if (format.equals("dd/MM/yyyy HH24:MI:SS"))
			return "dd-MM-yyyy HH:MM:SS";
		else if (format.equals("%d/%m/%Y"))
			return "dd-MM-yyyy";
		else if (format.equals("%d-%m-%Y"))
			return "dd-MM-yyyy";
		else if (format.equals("%d-%m-%Y %h:%i:%s"))
			return "dd-MM-yyyy HH:MM:SS";
		else
			return format;
	}

}
