package patterns.config.framework.delegate;

import java.io.Serializable;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.InitialContext;

public class MessageDrivenBeanInvoker {

	public static void invokeMDB(String connectionFactoryJNDIName, String queueJNDIName, Serializable parameterToMDB) {
		QueueSession session = null;
		try {
			InitialContext context = new InitialContext();
			QueueConnectionFactory factory = (QueueConnectionFactory) context.lookup(connectionFactoryJNDIName);
			QueueConnection connection = factory.createQueueConnection();
			session = (QueueSession) connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Queue queue = (Queue) context.lookup(queueJNDIName);
			QueueSender sender = session.createSender(queue);
			ObjectMessage message = session.createObjectMessage();
			message.setObject(parameterToMDB);
			sender.send(message);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (JMSException e) {
					e.printStackTrace();
				}
			}
		}

	}
}
