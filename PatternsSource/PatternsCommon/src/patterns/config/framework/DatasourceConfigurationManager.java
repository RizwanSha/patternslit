package patterns.config.framework;

import java.sql.ResultSet;

import patterns.config.framework.database.utils.DBContext;
import patterns.config.framework.database.utils.DBUtil;

public class DatasourceConfigurationManager extends ConfigurationManager {

	private static String dataSource = null;
	private static String schemaName = null;

	public static final String getDatabaseSource() {
		if (dataSource != null) {
			return dataSource;
		}
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			String sqlQuery = "SELECT DSRC_SERVER_TYPE FROM DSOURCE WHERE DSRC_STATUS='1'";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				dataSource = rs.getString("DSRC_SERVER_TYPE");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return dataSource;
	}

	public static final String getSchemaName() {
		if (schemaName != null) {
			return schemaName;
		}
		DBContext dbContext = new DBContext();
		DBUtil util = dbContext.createUtilInstance();
		try {
			String sqlQuery = "SELECT DSRC_NAME FROM DSOURCE WHERE DSRC_STATUS='1' ";
			util.reset();
			util.setMode(DBUtil.PREPARED);
			util.setSql(sqlQuery);
			ResultSet rs = util.executeQuery();
			if (rs.next()) {
				schemaName = rs.getString("DSRC_NAME");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbContext.close();
		}
		return schemaName;
	}

}